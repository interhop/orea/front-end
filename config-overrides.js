// allows to prevent "Error: Can't resolve 'fs'" when npm run build
module.exports = function override(config, env) {
  config.resolve.fallback = {
    fs: false
  }
  return config
}
