import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { Delete as DeleteIcon, Cancel as DenyIcon, TaskAlt as ValidateIcon } from '@mui/icons-material'
import { Unstable_Grid2 as Grid, Skeleton } from '@mui/material'

import AccessForm from 'components/Accesses/AccessForm/AccessForm'
import AccessesList from 'components/Accesses/AccessesList/AccessesList'
import ViewTitle from 'components/Basic/ViewTitle/ViewTitle'
import { isAccessValid } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { denyAccess, fetchAccesses, removeAccess, validateAccess } from 'state/accesses'
import { Access } from 'types'

const Accesses = () => {
  const { accesses, me } = useAppSelector((state) => ({
    accesses: state.accesses,
    me: state.me.user
  }))
  const dispatch = useDispatch<AppDispatch>()

  useEffect(() => {
    if (accesses.status !== 'loading' && accesses.ids.length === 0) dispatch(fetchAccesses({}))
  }, [])

  const deleteAccess = (id: string) => {
    dispatch(removeAccess(id))
  }

  const deny = (id: string) => {
    dispatch(denyAccess(id))
  }

  const validate = (id: string) => {
    dispatch(validateAccess(id))
  }

  const myAccesses = _.values(accesses.entities).filter(_.matches({ user: me?.uuid })) as Access[]
  const myValidatedAccesses = myAccesses.filter(_.matches({ status: 'validated' }))
  const myPendingAccesses = myAccesses.filter(_.matches({ status: 'created' }))
  const myDeniedAccesses = myAccesses.filter(_.matches({ status: 'denied' }))

  const myAdminAccesses = myValidatedAccesses.filter(_.matches({ rightReview: true })).filter(isAccessValid)
  const accessesToReview = _.values(accesses.entities).filter(
    (a) =>
      a &&
      a.status === 'created' &&
      _.includes(
        myAdminAccesses.map((aa) => aa.intensiveCareService),
        a.intensiveCareService
      )
  ) as Access[]

  return (
    <>
      <Grid container sm={12} justifyContent="center">
        <ViewTitle title="Gestion des accès" />
      </Grid>

      {accesses.status === 'loading' ? (
        <>
          <Skeleton variant="rounded" width="100%" height={90} />
          <Skeleton variant="rounded" width="100%" height={90} />
        </>
      ) : (
        <>
          <AccessesList accesses={myValidatedAccesses} title="Mes accès" type="validated" />
          <AccessesList
            accesses={myPendingAccesses}
            title="En attente"
            type="pending"
            actions={[{ callback: deleteAccess, Icon: DeleteIcon }]}
          />
          <AccessesList
            accesses={myDeniedAccesses}
            title="Accès refusés"
            type="denied"
            actions={[{ callback: deleteAccess, Icon: DeleteIcon }]}
          />
          <AccessesList
            accesses={accessesToReview}
            title="Accès en attentes de validation"
            type="toReview"
            actions={[
              { callback: deny, Icon: DenyIcon },
              { callback: validate, Icon: ValidateIcon }
            ]}
          />
          <AccessForm />
        </>
      )}
    </>
  )
}

export default Accesses
