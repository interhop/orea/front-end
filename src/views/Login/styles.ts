import { alpha, styled } from '@mui/material/styles'
import BackgroundLogin from 'assets/images/loginImage.png'
import BackgroundLoginNoBg from 'assets/images/loginNoBg.png'
import Logo from 'assets/images/logoOrea.png'

const PREFIX = 'LoginView'
export const classes = {
  root: `${PREFIX}-root`,
  loginPanel: `${PREFIX}-loginPanel`,
  hideIfSmallScreen: `${PREFIX}-hideIfSmallScreen`,
  image: `${PREFIX}-image`,
  form: `${PREFIX}-form`,
  bienvenue: `${PREFIX}-bienvenue`,
  logo: `${PREFIX}-logo`,
  section: `${PREFIX}-section`,
  input: `${PREFIX}-input`,
  inputLabel: `${PREFIX}-inputLabel`,
  submit: `${PREFIX}-submit`,
  mention: `${PREFIX}-mention`
}

export const Root = styled('div')(({ theme }) => ({
  [`&.${classes.root}`]: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  [`& .${classes.image}`]: {
    backgroundImage: `url(${BackgroundLoginNoBg})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    borderTopLeftRadius: '24px',
    borderBottomLeftRadius: '24px',
    height: '100%'
  },
  [`& .${classes.hideIfSmallScreen}`]: {
    [theme.breakpoints.down('sm')]: {
      height: 0
    }
  },
  [`& .${classes.loginPanel}`]: {
    width: '80%',
    height: '80%',
    opacity: 0.8,
    backgroundColor: alpha(theme.palette.primary.light, 1),
    borderRadius: '24px',
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: '90%',
      backgroundImage: `url(${BackgroundLogin})`,
      backgroundPosition: 'center center',
      marginTop: 5,
      marginBottom: 5,
      padding: 2
    }
  },
  [`& .${classes.form}`]: {
    width: '100%',
    padding: theme.spacing(5),
    margin: theme.spacing(5, 0, 1)
  },
  [`& .${classes.bienvenue}`]: {
    width: 'auto',
    borderRadius: 12,
    margin: '0.5rem',
    padding: '1rem',
    backgroundColor: alpha(theme.palette.background.paper, 0.2),
    color: theme.palette.primary.contrastText,
    [theme.breakpoints.down('sm')]: {
      color: theme.palette.primary.dark,
      backgroundColor: alpha(theme.palette.background.paper, 0.8)
    }
  },
  [`& .${classes.logo}`]: {
    backgroundImage: `url(${Logo})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    marginBottom: theme.spacing(5),
    width: '100%',
    maxWidth: 'calc(min(200px, 40vw))',
    aspectRatio: '1/1'
  },
  [`& .${classes.section}`]: {
    margin: theme.spacing(5, 0, 5),
    padding: theme.spacing(4),
    width: '100%',
    backgroundColor: alpha(theme.palette.background.paper, 0.2),
    borderRadius: '12px',
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)',
    [theme.breakpoints.down('sm')]: {
      backgroundColor: alpha(theme.palette.background.paper, 0.6)
    }
  },
  [`& .${classes.input}`]: {
    margin: theme.spacing(2, 0, 2)
  },
  [`& .${classes.inputLabel}`]: {
    opacity: 0.5,
    [`&.Mui-focused`]: {
      color: theme.palette.secondary.dark,
      opacity: 1
    }
  },
  [`& .${classes.submit}`]: {
    margin: theme.spacing(2),
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    width: '100%',
    maxWidth: 200,
    maxHeight: '10vh',
    aspectRatio: '5/2',
    borderRadius: '24px'
  },
  [`& .${classes.mention}`]: {
    marginTop: '8px'
  }
}))
