import React, { FormEventHandler, useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Navigate, useSearchParams } from 'react-router-dom'

import { Visibility, VisibilityOff } from '@mui/icons-material'
import Box from '@mui/material/Box'
// import Button from '@mui/material/Button'
// import Dialog from '@mui/material/Dialog'
// import DialogActions from '@mui/material/DialogActions'
// import DialogContent from '@mui/material/DialogContent'
// import DialogContentText from '@mui/material/DialogContentText'
// import DialogTitle from '@mui/material/DialogTitle'
import FormControl from '@mui/material/FormControl'
import Grid from '@mui/material/Unstable_Grid2'
import IconButton from '@mui/material/IconButton'
import InputAdornment from '@mui/material/InputAdornment'
import InputLabel from '@mui/material/InputLabel'
import OutlinedInput, { OutlinedInputProps } from '@mui/material/OutlinedInput'
import Typography from '@mui/material/Typography'
import Skeleton from '@mui/material/Skeleton'

import { classes, Root } from './styles'
import { AppDispatch, useAppSelector } from 'state'
import { login } from 'state/me'
import { LoadingButton } from '@mui/lab'
import { retrieveState } from '../../state/backend'

/*
const LegalMentionDialog: React.FC<{ open: boolean; setOpen: (o: boolean) => void }> = ({ open, setOpen }) => {
  const _setOpen = () => {
    if (setOpen && typeof setOpen === 'function') {
      setOpen(false)
    }
  }

  return (
    <Dialog open={open} onClose={_setOpen}>
      <DialogTitle>Mention légale</DialogTitle>
      <DialogContent>
        <DialogContentText align="justify">
          {/!*
          L’usage de Orea est soumis au respect des règles d’accès aux données de santé définies par la Commission
          Médicale d’Etablissement de l’AP-HP disponibles à l’adresse recherche-innovation.aphp.fr.
          *!/}
        </DialogContentText>
        <DialogContentText>
          En appuyant sur le bouton « OK », vous acceptez ces conditions d’utilisation. Les données relatives à votre
          connexion et à vos actions sur l’application (date, heure, type d’action), sont enregistrées et traitées pour
          des finalités de sécurité du système d’information et afin de réaliser des statistiques d’utilisation de
          l’application.
        </DialogContentText>
        <DialogContentText>
          {/!*
          Elles sont destinées à l’équipe projet de la DSI et sont conservées dans des fichiers de logs pendant 3 ans.
          Vous pouvez exercer votre droit d’accès et de rectification aux informations qui vous concernent, en écrivant
          à la déléguée à la protection des données de l’AP-HP à l’adresse protection.donnees.dsi@aphp.fr.
          *!/}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setOpen(false)}>OK</Button>
      </DialogActions>
    </Dialog>
  )
}
*/

const LoginInput: React.FC<Partial<OutlinedInputProps>> = (props) => (
  <FormControl color="secondary" fullWidth variant="outlined" className={classes.input}>
    {props.label && <InputLabel className={classes.inputLabel}>{props.label}</InputLabel>}
    <OutlinedInput required {...props} />
  </FormControl>
)

const LoginPanel: React.FC<any> = () => {
  const [showPassword, setShowPassword] = useState<boolean>(false)

  return (
    <Box className={classes.section}>
      <LoginInput name="username" label="Identifiant" id="identifiant" autoComplete="Identifiant" autoFocus />
      <LoginInput
        name="password"
        label="Votre mot de passe"
        id="password"
        type={showPassword ? 'text' : 'password'}
        autoComplete="current-password"
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={() => setShowPassword(!showPassword)}
              onMouseDown={(event) => event.preventDefault()}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        }
      />
    </Box>
  )
}

const DemoPanel: React.FC<any> = () => (
  <Typography className={classes.bienvenue} fontWeight="bold">
    Version de démonstration.
  </Typography>
)

const LoginForm: React.FC<any> = () => {
  const dispatch = useDispatch<AppDispatch>()
  const [searchParams] = useSearchParams()
  const next = searchParams.get('next')

  const { me, backend } = useAppSelector((state) => ({
    me: state.me,
    backend: state.backend
  }))

  useEffect(() => {
    if (backend.status !== 'loading' && !backend.backend) dispatch(retrieveState())
  }, [])

  const onSubmit: FormEventHandler = (e) => {
    e.preventDefault()
    const target = e.currentTarget as HTMLFormElement & {
      username: { value: string }
      password: { value: string }
    }

    dispatch(
      login({
        username: backend.backend?.demoUsername || target.username.value,
        password: backend.backend?.demoUsername || target.password.value
      })
    )
  }

  return me.user ? (
    <Navigate to={next ?? '/'} />
  ) : (
    <form className={classes.form} noValidate onSubmit={onSubmit}>
      <Grid container direction="column" alignItems="center">
        <div className={classes.logo} />

        <Typography className={classes.bienvenue} fontWeight="bold">
          Bienvenue sur Orea
        </Typography>

        {backend.status === 'loading' ? (
          <Skeleton variant="rounded" width="full" height={180} />
        ) : backend.backend?.demoUsername ? (
          <DemoPanel />
        ) : (
          <LoginPanel />
        )}
        {/*<Typography align="center" className={classes.mention}>*/}
        {/*  <Link*/}
        {/*    href="#"*/}
        {/*    sx={{ color: th.palette.error.light }}*/}
        {/*    // onClick={() => setOpen(true)}*/}
        {/*  >*/}
        {/*    En cliquant sur &quot;connexion&quot;, vous acceptez la mention légale.*/}
        {/*  </Link>*/}
        {/*</Typography>*/}

        <LoadingButton
          loading={me.status === 'loading'}
          type="submit"
          fullWidth
          variant="contained"
          className={classes.submit}
          id="connection-button-submit"
          disabled={backend.status === 'loading'}
        >
          {backend.backend?.demoUsername ? 'Découvrir' : 'Connexion'}
        </LoadingButton>
      </Grid>

      {/*<Box mt={10} alignContent="center">*/}
      {/*  <Grid container justifyContent="center">*/}
      {/*    <Link href="https://eds.aphp.fr">/!*<img className={classes.logoAPHP} src={logoAPHP} alt="Footer" />*!/</Link>*/}
      {/*  </Grid>*/}
      {/*</Box>*/}
    </form>
  )
}

const Login: React.FC<any> = () => {
  return (
    <Root className={classes.root}>
      <Grid container alignItems="center" justifyContent="space-between" className={classes.loginPanel}>
        <Grid sm={8} className={`${classes.hideIfSmallScreen} ${classes.image}`} />
        <Grid sm={4}>
          <LoginForm />
        </Grid>
      </Grid>
    </Root>
  )
}

export default Login
