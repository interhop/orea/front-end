// export {}
import React from 'react'
import { Page, Text, View, StyleSheet } from '@react-pdf/renderer'
import { dateTimeToStr } from 'shared/utils/date'

const pdfStyles = StyleSheet.create({
  title: {
    width: '100%',
    alignItems: 'center',
    top: 1
  },
  titleText: {
    fontSize: 10,
    marginTop: '10px'
  },
  footer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 5,
    left: 5,
    right: 5
  },
  footerText: {
    fontSize: 8
  }
})

const PdfBedsTemplate = ({ serviceName, components }) => {
  return (
    <Page size="A4" wrap>
      <View fixed style={pdfStyles.title}>
        <Text render={() => `${serviceName.toUpperCase()}`} style={pdfStyles.titleText} />
      </View>

      {components}

      <View fixed style={pdfStyles.footer}>
        <Text style={pdfStyles.footerText} render={() => `${dateTimeToStr(new Date())}`} />
        <Text
          style={pdfStyles.footerText}
          render={
            ({ pageNumber }) => `${pageNumber}` // / ${totalPages}`
          }
        />
      </View>
    </Page>
  )
}

export default PdfBedsTemplate
