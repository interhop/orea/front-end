import React, { useEffect, useState } from 'react'
import { Navigate, useParams } from 'react-router-dom'

import { Box, Unstable_Grid2 as Grid } from '@mui/material'

import { classes, Root } from './styles'
import AddLabelDial from 'components/Dialogs/AddLabelDIal'
import ListLabels from 'components/Basic/ListLabels/ListLabels'
import MarkdownDial from 'components/Dialogs/MarkdownDial'
import DemographicDisplay from 'components/BedStay/DemographicData/DemographicDisplay'
import Detections from 'components/BedStay/Detections/Detections'
import Failures from 'components/BedStay/Failures/Failures'
import GeneralNotes from 'components/BedStay/GeneralNotes/GeneralNotes'
import Severity from 'components/BedStay/Severity/Severity'
import StayNote from 'components/BedStay/StayNote/StayNote'
import BedTitle from 'components/BedStay/Title/Title'
import BedDialog from 'components/Dialogs/BedDial'
import FailureUpdateDial from 'components/Dialogs/FailureUpdateDial'
import { useAppDispatch, useAppSelector } from 'state'
import { fetchStay } from 'state/stays'
import { fetchBed } from 'state/beds'
import { Bed as BedType, Patient, Stay } from 'types'

const Bed: React.FC<any> = () => {
  const { bedId } = useParams<{
    bedId?: string
  }>()

  const { beds, stays } = useAppSelector((state) => ({
    beds: state.beds,
    stays: state.stays
  }))

  const [stateBed, setStateBed] = useState<BedType | undefined>(beds.entities[bedId || ''])
  const [stateStay, setStateStay] = useState<Stay | undefined>()

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (bedId && !(bedId in beds.entities) && beds.status !== 'loading') {
      dispatch(fetchBed(bedId))
    }
  }, [])

  useEffect(() => {
    bedId && setStateBed(beds.entities[bedId])
  }, [beds, bedId])

  useEffect(() => {
    const stayId = stateBed?.currentPatientStay
    if (stayId) {
      if (!(stayId in stays.entities) && stays.status !== 'loading') {
        dispatch(fetchStay(stayId))
      } else {
        setStateStay(stays.entities[stayId])
      }
    }
  }, [stateBed, stays])

  if (!bedId) return <Navigate to="/" />
  if (!stateBed) return <></>

  return (
    <Root>
      <Grid container width="100%" justifyContent="space-between" alignItems="flex-start">
        <Grid xs={12} container alignItems="center" justifyContent="space-evenly">
          <BedTitle bed={stateBed} />
        </Grid>

        {stateBed.currentPatientStay && (
          <>
            <Grid xs={12} md={8} className={classes.cardContainer}>
              <Box className={classes.card} style={{ width: '100%' }}>
                <Grid container justifyContent="flex-start">
                  <Grid xs={12} sm={8}>
                    <Grid>
                      <DemographicDisplay stayId={stateBed.currentPatientStay} />
                    </Grid>
                    {stateStay && (
                      <Grid xs={12}>
                        <ListLabels title="Antécédents" patient={stateStay?.patient as Patient} field="antecedents" />
                      </Grid>
                    )}
                    {stateStay && (
                      <Grid xs={12}>
                        <ListLabels title="Allergies" patient={stateStay?.patient as Patient} field="allergies" />
                      </Grid>
                    )}
                    <AddLabelDial />
                  </Grid>
                  <Grid container xs={12} sm={4} direction="column" justifyContent="flex-start">
                    <Grid>
                      <Severity stayId={stateBed.currentPatientStay} />
                    </Grid>
                    <Grid>
                      <Failures stayId={stateBed.currentPatientStay} />
                    </Grid>
                    <Grid className={classes.card}>
                      <Detections stayId={stateBed.currentPatientStay} />
                    </Grid>
                  </Grid>
                </Grid>
              </Box>
            </Grid>

            <Grid container xs={12} sm={4} spacing={1} className={classes.card}>
              <GeneralNotes stayId={stateBed.currentPatientStay} />
            </Grid>

            <Grid xs={12} sm={4} md={6} className={classes.card}>
              <StayNote stayId={stateBed.currentPatientStay} type={'rdh'} />
            </Grid>

            <Grid xs={12} sm={4} md={6} className={classes.card}>
              <StayNote stayId={stateBed.currentPatientStay} type={'evolution'} />
            </Grid>
          </>
        )}
      </Grid>
      <BedDialog />
      <FailureUpdateDial />
      <MarkdownDial />
    </Root>
  )
}

export default Bed
