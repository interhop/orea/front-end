export type Question = {
  subject: string
  answer: string
}

export type Section = {
  subject: string
  questions: Question[]
}

const faq: Section[] = [
  {
    subject: 'Données',
    questions: [
      {
        subject: 'Où sont stockées les données ?',
        answer:
          "Cela dépend de votre service. Elles sont généralement sur le serveur de l'hôpital, ou bien du groupe hospitalier (ex.: AP-HP)"
      },
      {
        subject: 'Les données sont-elles traitées ?',
        answer:
          "Pas du tout, les données servent exclusivement aux utilisateurs, dans le cadre de la transmission d'informations. Il est prévu de mettre en place une date limite sur toutes les informations sur les séjours des patients."
      },
      {
        subject: 'Les données sont-elles en sécurité ?',
        answer:
          "Actuellement, la sécurité est au niveau de celle du serveur d'hébergement. Il est envisagé d'ajouter une option de chiffrement à l'avenir pour parfaire celle-ci."
      }
    ]
  },
  {
    subject: 'Développeurs',
    questions: [
      {
        subject: 'Qui sommes-nous ?',
        answer:
          'Cette application est fournie par Interhop, une association de développeurs proposant des solutions pour la santé sous licences libre de droits.'
      }
    ]
  }
]

export default faq
