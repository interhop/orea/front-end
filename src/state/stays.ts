import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'
import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'
import { AxiosResponse, isAxiosError } from 'axios'
import _ from 'lodash'

import { fetchBeds, updateBed } from './beds'
import { AppDispatch, RootState } from './index'
import { logout } from './me'
import services from 'services'
import {
  BackValidationError,
  baseApiModelSelectId,
  BasicFilter,
  FrontInput,
  Patient,
  PatientDetection,
  PatientFailure,
  PatientNote,
  Stay,
  TreatmentLimitation
} from 'types'

export type StaysState = {
  status: 'idle' | 'loading'
  patientStatus: 'idle' | 'loading'
  entities: Dictionary<Stay>
  ids: any[]
}

const stayEntityAdapter: EntityAdapter<Stay> = createEntityAdapter({
  selectId: baseApiModelSelectId
})
const defaultInitialState: StaysState = stayEntityAdapter.getInitialState({
  status: 'idle',
  patientStatus: 'idle'
})

const fetchStay = createAsyncThunk<Stay, string, {}>(
  'stays/fetchStay',
  async (stayId) => await services.stays.fetchStay(stayId).then((resp) => resp.data)
)

const fetchStays = createAsyncThunk<Stay[], BasicFilter<Stay>, {}>(
  'stays/fetchStays',
  async (params?) => await services.stays.fetchStays(params).then(({ data }) => data.results)
)

const updatePatient = createAsyncThunk<
  Patient,
  { patientId: string; data: Partial<Patient> },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>(
  'stays/updatePatient',
  async ({ patientId, data }, thunkAPI) =>
    await services.patients.updatePatient(patientId, data).then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
)

const updateStay = createAsyncThunk<Stay, { stayId: string; data: Partial<Stay> }, {}>(
  'stays/updateStay',
  async ({ stayId, data }) => await services.stays.updateStay(stayId, data).then((resp) => resp.data)
)

const createDetection = createAsyncThunk<
  PatientDetection,
  { detection: Partial<PatientDetection> },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>(
  'stays/createDetection',
  async ({ detection }, thunkAPI) =>
    await services.stays
      .createDetection(detection)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
)

const createFailure = createAsyncThunk<
  PatientFailure,
  { failure: FrontInput<PatientFailure> },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>(
  'stays/createFailure',
  async ({ failure }, thunkAPI) =>
    await services.stays
      .createFailureMeasure(failure)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
)

const editFailure = createAsyncThunk<
  PatientFailure,
  { failureId: string; failure: Partial<PatientFailure> },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>(
  'stays/editFailure',
  async ({ failureId, failure }, thunkAPI) =>
    await services.stays
      .updateFailureMeasure(failureId, failure)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
)

const editStayTreatmentLimitation = createAsyncThunk<
  TreatmentLimitation,
  { newLat: Partial<TreatmentLimitation>; oldLatId?: string },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>(
  'stays/editStayTreatmentLimitation',
  async ({ newLat, oldLatId }, thunkAPI) =>
    await (oldLatId
      ? services.stays.updateTreatmentLimitation(oldLatId, newLat)
      : services.stays.createTreatmentLimitation(newLat)
    )
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
)

const editBedStayNote = createAsyncThunk<
  PatientNote,
  { newNote: Partial<PatientNote>; oldNoteId?: string },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>(
  'beds/editBedStayNote',
  async ({ newNote, oldNoteId }, thunkAPI) =>
    await (oldNoteId ? services.stays.updateNote(oldNoteId, newNote) : services.stays.createNote(newNote))
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
)

const closeBedStay = createAsyncThunk<
  Stay,
  { stay: Stay },
  { rejectValue: AxiosResponse<BackValidationError> | undefined; dispatch: AppDispatch; state: RootState }
>('beds/closeBedStay', async ({ stay }, thunkAPI) => {
  const resp = await services.stays
    .closeStay(stay.uuid)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
  if (!('uuid' in resp)) return resp
  thunkAPI.dispatch(updateBed({ id: stay.bed, changes: { currentPatientStay: null } }))
  return resp
})

const moveBedStay = createAsyncThunk<
  Stay,
  { stay: Stay; newBedId: string },
  { rejectValue: AxiosResponse<BackValidationError> | undefined; dispatch: AppDispatch }
>('beds/moveBedStay', async ({ stay, newBedId }, { dispatch, rejectWithValue }) => {
  const resp = await services.stays
    .moveStay(stay.uuid, newBedId)
    .then((resp) => {
      if (isAxiosError(resp)) return rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => rejectWithValue(e))

  // todo : think about better check and generalise it
  // test without 'return' rejectWithValue
  !('payload' in resp) &&
    dispatch(fetchBeds({ forceReloadStays: true, filters: { uuid: [newBedId, stay.bed].join(',') } }))
  return resp
})

const createPatientAndStay = createAsyncThunk<
  Stay,
  { patient: Partial<Patient> & { stayStartDate?: string }; bedId: string },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('beds/createPatientAndStay', async ({ patient, bedId }, thunkAPI) => {
  const p = await services.patients
    .createPatient(patient)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
  if (!('uuid' in p)) return p

  const resp = await services.stays
    .createStay({ patient: p.uuid || '', bed: bedId, startDate: patient.stayStartDate })
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
  if (!('uuid' in resp)) return resp

  thunkAPI.dispatch(updateBed({ id: bedId, changes: { currentPatientStay: resp.uuid } }))
  return resp
})

const staysSlice = createSlice({
  name: 'stays',
  initialState: defaultInitialState as StaysState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchStay.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchStay.fulfilled, (state, action) => {
        stayEntityAdapter.upsertOne(state, action)
        state.status = 'idle'
      })
      .addCase(fetchStay.rejected, (state) => ({ ...state, status: 'idle' }))
      .addCase(fetchStays.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchStays.fulfilled, (state, action) => {
        stayEntityAdapter.upsertMany(state, action)
        state.status = 'idle'
      })
      .addCase(fetchStays.rejected, (state) => ({ ...state, status: 'idle' }))
      .addCase(updatePatient.pending, (state) => ({ ...state, patientStatus: 'loading' }))
      .addCase(updatePatient.fulfilled, (state, action) => {
        const stay = action.payload.currentStay ? state.entities[action.payload.currentStay] : undefined
        if (stay) {
          stayEntityAdapter.upsertOne(state, { ...stay, patient: action.payload })
          state.patientStatus = 'idle'
        }
      })
      .addCase(updatePatient.rejected, (state) => ({ ...state, patientStatus: 'idle' }))
      .addCase(updateStay.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(updateStay.fulfilled, (state, action) => {
        stayEntityAdapter.upsertOne(state, action.payload)
        state.status = 'idle'
      })
      .addCase(updateStay.rejected, (state) => ({ ...state, status: 'idle' }))
      .addCase(createDetection.fulfilled, (state, action) => {
        const stay = state.entities[action.payload.stay as string]
        if (!stay) return state
        stayEntityAdapter.upsertOne(state, { ...stay, detections: [...(stay.detections || []), action.payload] })
      })
      .addCase(createFailure.fulfilled, (state, action) => {
        const stay = state.entities[action.payload.stay as string]
        if (!stay) return state
        stayEntityAdapter.upsertOne(state, {
          ...stay,
          failureMeasures: [...(stay.failureMeasures || []), action.payload]
        })
      })
      .addCase(editFailure.fulfilled, (state, action) => {
        const stay = state.entities[action.payload.stay as string]
        if (!stay) return state
        const failure = (stay.failureMeasures || []).find(_.matches({ uuid: action.payload.uuid }))
        if (!failure) return state
        _.assign(failure, action.payload)
      })
      .addCase(editStayTreatmentLimitation.fulfilled, (state, action) => {
        if (!action.payload) return state
        const stay = _.find(_.values(state.entities), _.matches({ uuid: action.payload.stay }))
        if (!stay) return state
        _.assign(stay, { treatmentLimitations: [...(stay?.treatmentLimitations ?? []), action.payload] })
      })
      .addCase(createPatientAndStay.fulfilled, stayEntityAdapter.upsertOne)
      .addCase(editBedStayNote.pending, (state, action) => {
        if (!action.meta.arg.oldNoteId) return state
        const stay: Stay | undefined = _.values(state.entities).find(
          (s) => s && (s.notes ?? []).some(_.matches({ uuid: action.meta.arg.oldNoteId }))
        )
        if (!stay) return state

        const note = _.find(stay.notes ?? [], _.matches({ uuid: action.meta.arg.oldNoteId }))
        _.assign(note, { loading: true })
      })
      .addCase(editBedStayNote.fulfilled, (state, action) => {
        if (!action.payload) return state
        const stay = _.find(_.values(state.entities), _.matches({ uuid: action.payload.stay }))
        if (!stay) return state

        const note = _.find(stay?.notes ?? [], _.matches({ uuid: action.payload.uuid }))
        if (!note) {
          stay.notes = [...(stay.notes ?? []), action.payload]
        } else {
          _.assign(note, { ...action.payload, loading: false })
        }
      })
      .addCase(closeBedStay.fulfilled, stayEntityAdapter.upsertOne)
  }
})

export default staysSlice.reducer
export {
  fetchStay,
  fetchStays,
  updatePatient,
  updateStay,
  createDetection,
  createFailure,
  editFailure,
  editBedStayNote,
  closeBedStay,
  createPatientAndStay,
  moveBedStay,
  editStayTreatmentLimitation
}
