import { AxiosResponse, isAxiosError } from 'axios'

import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'
import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'

import { logout } from './me'
import services from 'services'
import { IssueFilters } from 'services/issues'
import { Issue, BackValidationError, State, PostedIssue } from 'types'

export type IssuesState = {
  status: 'idle' | 'loading'
  entities: Dictionary<State<Issue>>
  ids: any[]
}

const issueEntityAdapter: EntityAdapter<State<Issue>> = createEntityAdapter({
  selectId: ({ id }: Issue): string => id.toString()
})

const defaultInitialState: IssuesState = issueEntityAdapter.getInitialState({
  status: 'idle'
})

const fetchIssues = createAsyncThunk<
  Issue[],
  IssueFilters,
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('issues/fetchIssues', async (params, thunkAPI) => {
  return await services.issues
    .fetchIssues(params)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data.results
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const createIssue = createAsyncThunk<
  Issue,
  Partial<PostedIssue>,
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('issues/createIssue', async (issue, thunkAPI) => {
  return await services.issues
    .createIssue(issue)
    .then((resp) => {
      if (isAxiosError(resp)) {
        if (resp.response) {
          // remove non-serializable values for this FormData request
          resp.response.config.adapter = undefined
          resp.response.config.data = undefined
          resp.response.config.transformResponse = undefined
          resp.response.config.transformRequest = undefined
          resp.response.config.validateStatus = undefined
          resp.response.request = undefined
        }
        return thunkAPI.rejectWithValue(resp.response)
      }
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const voteIssue = createAsyncThunk<Issue, Issue, { rejectValue: AxiosResponse<BackValidationError> | undefined }>(
  'issues/voteIssue',
  async (issue, thunkAPI) => {
    return await services.issues
      .voteIssue(issue.projectId, issue.iid)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
  }
)

const unvoteIssue = createAsyncThunk<Issue, Issue, { rejectValue: AxiosResponse<BackValidationError> | undefined }>(
  'issues/unvoteIssue',
  async (issue, thunkAPI) => {
    return await services.issues
      .unvoteIssue(issue.projectId, issue.iid)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
  }
)

const setEntityLoading = (state: IssuesState, id: string) =>
  issueEntityAdapter.upsertOne(state, {
    ...state.entities[id],
    loading: true
  } as State<Issue>)

const setEntityNotLoading = (state: IssuesState, id: string) =>
  issueEntityAdapter.upsertOne(state, {
    ...state.entities[id],
    loading: false
  } as State<Issue>)

const issuesSlice = createSlice({
  name: 'issues',
  initialState: defaultInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchIssues.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchIssues.fulfilled, (state, action) => {
        issueEntityAdapter.upsertMany(state, action)
        state.status = 'idle'
      })
      .addCase(fetchIssues.rejected, (state) => ({
        ...state,
        status: 'idle'
      }))
      .addCase(createIssue.fulfilled, issueEntityAdapter.upsertOne)
      .addCase(voteIssue.pending, (state, action) => setEntityLoading(state, action.meta.arg.id.toString()))
      .addCase(unvoteIssue.pending, (state, action) => setEntityLoading(state, action.meta.arg.id.toString()))
      .addCase(voteIssue.fulfilled, issueEntityAdapter.setOne)
      .addCase(unvoteIssue.fulfilled, issueEntityAdapter.setOne)
      .addCase(voteIssue.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg.id.toString()))
      .addCase(unvoteIssue.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg.id.toString()))
  }
})

export default issuesSlice.reducer
export { fetchIssues, createIssue, voteIssue, unvoteIssue }
