import _ from 'lodash'
import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'
import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'

import { fetchBeds } from './beds'
import { AppDispatch, RootState } from './index'
import { logout } from './me'
import services from 'services'
import { baseApiModelSelectId, Unit } from 'types'

export type UnitsState = {
  status: 'idle' | 'loading'
  entities: Dictionary<Unit>
  ids: any[]
}

const unitEntityAdapter: EntityAdapter<Unit> = createEntityAdapter({
  selectId: baseApiModelSelectId
})
const defaultInitialState: UnitsState = unitEntityAdapter.getInitialState({
  status: 'idle'
})

const fetchUnits = createAsyncThunk<Unit[], Partial<Unit>, { state: RootState; dispatch: AppDispatch }>(
  'units/fetchUnits',
  async (params, thunkAPI) => {
    const resp = await services.units.fetchUnits(params).then(({ data }) => data.results)
    const stateBeds = thunkAPI.getState().beds
    const missingBeds = resp.map((u) => _.difference(u.beds, stateBeds.ids))
    resp &&
      missingBeds.length &&
      stateBeds.status !== 'loading' &&
      thunkAPI.dispatch(fetchBeds({ filters: { uuid: _.concat(...missingBeds).join(',') } }))
    return resp
  }
)

const unitsSlice = createSlice({
  name: 'units',
  initialState: defaultInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchUnits.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchUnits.fulfilled, (state, action) => {
        unitEntityAdapter.upsertMany(state, action)
        state.status = 'idle'
      })
      .addCase(fetchUnits.rejected, (state) => ({
        ...state,
        status: 'idle'
      }))
    // .addCase(fetchServices.fulfilled, (state, action) => {
    //   unitEntityAdapter.upsertMany(state, _.concat(...(action.payload ?? []).map((s) => s.units ?? [])))
    //   state.status = 'idle'
    // })
    // .addCase(fetchBeds.fulfilled, (state, action) => {
    //   const dictStateBeds: Dictionary<Bed> = _.keyBy(_.concat(..._.values(state.entities).map((u) => u?.beds)), 'uuid')
    //   action.payload.data.results.forEach((b) => {
    //     if (b.uuid && b.uuid in dictStateBeds) {
    //       _.assign(dictStateBeds[b.uuid], b)
    //     }
    //   })
    // })
  }
})

export default unitsSlice.reducer
export { fetchUnits }
