import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'
import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'
import { AxiosResponse, isAxiosError } from 'axios'

import { logout } from './me'
import services from 'services'
import { UserFilters } from 'services/users'
import { baseApiModelSelectId, User } from 'types'

export type UsersState = {
  status: 'idle' | 'loading'
  entities: Dictionary<User>
  ids: any[]
}

const usersEntityAdapter: EntityAdapter<User> = createEntityAdapter({
  selectId: baseApiModelSelectId
})
const defaultInitialState: UsersState = usersEntityAdapter.getInitialState({
  status: 'idle'
})

const fetchUsers = createAsyncThunk<User[], UserFilters, { rejectValue: AxiosResponse<any> | undefined }>(
  'users/fetchUsers',
  async (params, thunkAPI) =>
    await services.users
      .fetchUsers(params)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data.results
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
)

const usersSlice = createSlice({
  name: 'users',
  initialState: defaultInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchUsers.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchUsers.fulfilled, (state, action) => {
        usersEntityAdapter.upsertMany(state, action)
        state.status = 'idle'
      })
      .addCase(fetchUsers.rejected, (state) => ({ ...state, status: 'idle' }))
  }
})

export default usersSlice.reducer
export { fetchUsers }
