import { baseApiModelSelectId, Bed, State } from 'types'
import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'
import { logout } from './me'
import _ from 'lodash'

import services from 'services'
import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'
import { AppDispatch, RootState } from './index'
import { fetchStays } from './stays'

export type BedsState = {
  status: 'idle' | 'loading'
  entities: Dictionary<State<Bed>>
  ids: any[]
}

const bedEntityAdapter: EntityAdapter<State<Bed>> = createEntityAdapter({
  selectId: baseApiModelSelectId
})
const defaultInitialState: BedsState = bedEntityAdapter.getInitialState({
  status: 'idle'
})

const fetchBed = createAsyncThunk<Bed, string, {}>(
  'beds/fetchBed',
  async (bedId) => await services.beds.fetchBed(bedId).then((resp) => resp.data)
)

const fetchBeds = createAsyncThunk<
  Bed[],
  { filters?: Partial<Bed>; forceReloadStays?: boolean } | undefined,
  { state: RootState; dispatch: AppDispatch }
>('beds/fetchBeds', async (params, thunkAPI) => {
  const resp = await services.beds.fetchBeds(params?.filters).then(({ data }) => data.results)

  const stateStays = thunkAPI.getState().stays
  const respStays = _.filter(_.map(resp, 'currentPatientStay'))
  const missingStays = params?.forceReloadStays ? respStays : _.difference(respStays, stateStays.ids)

  resp &&
    missingStays.length &&
    stateStays.status !== 'loading' &&
    thunkAPI.dispatch(
      fetchStays({
        uuid: missingStays.join(',')
      })
    )
  return resp
})

const bedsSlice = createSlice({
  name: 'beds',
  initialState: defaultInitialState,
  reducers: {
    updateBed: bedEntityAdapter.updateOne
  },
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchBed.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchBed.fulfilled, (state, action) => {
        bedEntityAdapter.upsertOne(state, action)
        state.status = 'idle'
      })
      .addCase(fetchBed.rejected, (state) => ({ ...state, status: 'idle' }))
      .addCase(fetchBeds.pending, (state, action) => {
        if (action.meta.arg?.filters?.uuid) {
          const fetchedIds = action.meta.arg?.filters?.uuid.split(',')
          const newIds = _.difference(fetchedIds, state.ids)
          if (!newIds.length) {
            bedEntityAdapter.updateMany(
              state,
              fetchedIds.map((id) => ({ id: id, changes: { loading: true } }))
            )
            return
          }
        }
        state.status = 'loading'
      })
      .addCase(fetchBeds.fulfilled, (state, action) => {
        bedEntityAdapter.upsertMany(
          state,
          action.payload.map((b) => ({ ...b, loading: false }))
        )
        state.status = 'idle'
      })
    // .addCase(fetchUnits.fulfilled, (state, action) => {
    //   bedEntityAdapter.upsertMany(state, _.concat(...(action.payload ?? []).map((u) => u.beds ?? [])))
    // })
    // .addCase(fetchServices.fulfilled, (state, action) => {
    //   const units = _.concat(...(action.payload ?? []).map((s) => s.units ?? []))
    //   bedEntityAdapter.upsertMany(state, _.concat(...(units ?? []).map((u) => u.beds ?? [])))
    // })
  }
})

export default bedsSlice.reducer
export { fetchBed, fetchBeds, bedEntityAdapter }
export const { updateBed } = bedsSlice.actions
