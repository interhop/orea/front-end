import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import _ from 'lodash'

import { fetchBeds } from './beds'
import { logout } from './me'

export type SelectionState = {
  beds: string[]
}

const defaultInitialState: SelectionState = {
  beds: []
}

const selectionSlice = createSlice({
  name: 'selection',
  initialState: defaultInitialState,
  reducers: {
    selectBeds: (state, action: PayloadAction<string[]>) => ({
      ...state,
      beds: _.uniq([...state.beds, ...action.payload])
    }),
    unselectBeds: (state, action: PayloadAction<string[]>) => ({
      ...state,
      beds: state.beds.filter((id) => !_.includes(action.payload, id))
    })
  },
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchBeds.fulfilled, (state, action) => {
        if (!state.beds.length)
          state.beds.push(
            ...(action.payload
              .filter(({ currentPatientStay }) => !!currentPatientStay)
              .map(_.property('uuid')) as string[])
          )
      })
  }
})

export default selectionSlice.reducer
export const { selectBeds, unselectBeds } = selectionSlice.actions
