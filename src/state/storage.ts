import localforage from 'localforage'

export const clearLocalStorage = () => {
  localStorage.removeItem('accessToken')
  localforage.removeItem('persist:me')
  localforage.removeItem('persist:selection')
  localforage.removeItem('persist:backend')
}
