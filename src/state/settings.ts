import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export type PrintSettingsComponent =
  | 'detections'
  | 'antecedents'
  | 'allergies'
  | 'recentDiseaseHistory'
  | 'evolution'
  | 'todoList'
  | 'lat'
  | 'dailyNotes'
  | 'failures'
  // | 'measuresTable'
  | 'writingSpace'

export type PrintSettings = {
  components?: PrintSettingsComponent[]
  newPageForUnit?: boolean
  bedsPerPage?: number
  onePatientPerPage?: boolean
}

type SettingsState = {
  nightMode: boolean
  todoMode: boolean
  printMode: boolean
  printSettings?: PrintSettings
}

const settingsSlice = createSlice({
  name: 'settings',
  initialState: {
    nightMode: false,
    todoMode: false,
    printMode: false,
    printSettings: {
      components: [
        'detections',
        'antecedents',
        'allergies',
        'recentDiseaseHistory',
        'evolution',
        'todoList',
        'lat',
        'dailyNotes',
        'failures',
        'writingSpace'
      ]
    }
  } as SettingsState,
  reducers: {
    toggleNightMode: (state) => ({ ...state, nightMode: !state.nightMode }),
    toggleTodoMode: (state) => ({ ...state, todoMode: !state.todoMode }),
    togglePrintMode: (state) => ({ ...state, printMode: !state.printMode }),
    setPrintSettings: (state, action: PayloadAction<Partial<PrintSettings>>) => ({
      ...state,
      printSettings: action.payload
    })
  }
})

export default settingsSlice.reducer
export const { toggleNightMode, toggleTodoMode, togglePrintMode, setPrintSettings } = settingsSlice.actions
