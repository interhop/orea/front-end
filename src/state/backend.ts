import { AxiosResponse, isAxiosError } from 'axios'
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import services from 'services'
import { getCsrfToken } from 'services/api'
import { BackValidationError, Backend } from 'types'

export type BackendState = {
  status: 'idle' | 'loading'
  backend: null | Backend
}

export const retrieveState = createAsyncThunk<
  Backend,
  undefined,
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('backendState/retrieveState', async (arg, thunkAPI) => {
  getCsrfToken() || (await services.user.getCsrfToken())
  return await services.backend
    .retrieveState()
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const backendStateSlice = createSlice({
  name: 'backendState',
  initialState: { status: 'idle', backend: null } as BackendState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(retrieveState.pending, (state) => ({ ...state, status: 'loading' }))
    builder.addCase(retrieveState.fulfilled, (state, action) => {
      state.backend = action.payload
      state.status = 'idle'
    })
    builder.addCase(retrieveState.rejected, (state) => {
      state.status = 'idle'
    })
  }
})

export default backendStateSlice.reducer
