import { configureStore } from '@reduxjs/toolkit'
import localforage from 'localforage'
import { persistStore, persistCombineReducers, persistReducer } from 'redux-persist'
import { FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE } from 'redux-persist/es/constants'

import accesses from './accesses'
import backend from './backend'
import beds from './beds'
import settings from './settings'
import intensiveCareServices from './intensiveCareServices'
import issues from './issues'
import me from './me'
import message from './message'
import modals from './modals'
import selection from './selection'
import stays from './stays'
import units from './units'
import users from './users'
import config from 'config'

const persistConfig = {
  key: 'root',
  storage: localforage,
  // blacklist: ['beds', 'intensiveCareServices', 'message', 'modals', 'stays', 'units'],
  whitelist: ['settings']
}

const reducer = {
  accesses,
  backend: persistReducer({ key: 'backend', storage: localforage, blacklist: ['status'] }, backend),
  beds,
  settings,
  intensiveCareServices,
  issues,
  me: persistReducer({ key: 'me', storage: localforage, blacklist: ['status'] }, me),
  message,
  modals,
  selection: persistReducer({ key: 'selection', storage: localforage, blacklist: ['status'] }, selection),
  stays,
  units,
  users
}

const persistedReducer = persistCombineReducers(persistConfig, reducer)

export const store = configureStore({
  reducer: persistedReducer,
  devTools: config.devMode,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
      }
    })
})

// export const store = createStore(persistedReducer, applyMiddleware(thunkMiddleware, logger))
export const persistor = persistStore(store)
