import React from 'react'
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon'

// https://www.svgrepo.com/svg/316313/female

const FemaleIcon: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon viewBox={'0 0 24 24'} {...props}>
      <path
        fill="none"
        d="
        M12 13
        C14.7614 13 17 10.7614 17 8
        C17 5.23858 14.7614 3 12 3
        C9.23858 3 7 5.23858 7 8
        C7 10.7614 9.23858 13 12 13
        Z
        M12 13
        L12 21
        M9 18
        L15 18"
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </SvgIcon>
  )
}

export default FemaleIcon
