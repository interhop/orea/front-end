import React from 'react'
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon'

// https://www.svgrepo.com/svg/316352/male

const MaleIcon: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon viewBox={'0 0 24 24'} {...props}>
      <path
        fill="none"
        d="
        M12 11
        C9.23858 11 7 13.2386 7 16
        C7 18.7614 9.23858 21 12 21
        C14.7614 21 17 18.7614 17 16
        C17 13.2386 14.7614 11 12 11
        Z
        M12 11
        V3
        M12 3
        L16 7
        M12 3
        L8 7"
        // stroke="#001A72"
        stroke="currentColor"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </SvgIcon>
  )
}

export default MaleIcon
