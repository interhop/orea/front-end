import React from 'react'
import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon'

// Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
const MovePatientIcon: React.FC<SvgIconProps> = (props) => {
  return (
    <SvgIcon viewBox={'0 0 490.667 490.667'} {...props}>
      <path d="M245.333,0C110.059,0,0,110.059,0,245.333s110.059,245.333,245.333,245.333s245.333-110.059,245.333-245.333 S380.608,0,245.333,0z M193.792,132.757l42.667-64c3.968-5.931,13.781-5.931,17.749,0l42.667,64 c2.176,3.264,2.389,7.488,0.533,10.944c-1.856,3.456-5.461,5.632-9.408,5.632h-85.333c-3.947,0-7.552-2.176-9.408-5.632 C191.403,140.245,191.616,136.021,193.792,132.757z M296.875,357.909l-42.667,64c-1.984,2.987-5.312,4.757-8.875,4.757 s-6.891-1.771-8.875-4.757l-42.667-64c-2.176-3.264-2.389-7.488-0.533-10.944s5.461-5.632,9.408-5.632H288 c3.947,0,7.552,2.176,9.408,5.632C299.264,350.421,299.051,354.645,296.875,357.909z M245.333,298.667 c-29.397,0-53.333-23.936-53.333-53.333S215.936,192,245.333,192s53.333,23.936,53.333,53.333S274.731,298.667,245.333,298.667z" />
    </SvgIcon>
  )
}

export default MovePatientIcon
