import React from 'react'
import { AxiosResponse } from 'axios'
import _ from 'lodash'

import convert from 'color-convert'
import { useTheme, useMediaQuery } from '@mui/material'
import { AlertColor } from '@mui/lab/Alert/Alert'

import { Access, Patient, User } from 'types'

/**
 * This callback type is called `requestCallback` and is displayed as a global symbol.
 *
 * @callback uiInform
 * @param {string} msg
 * @param {string} infoType either 'success', 'error', 'info' or 'warning'
 */

type ManageErrorInputs = {
  errResp?: AxiosResponse<any, any>
  uiInform: (msg: string, infoType: AlertColor) => any
}

export const manageError = ({ errResp, uiInform }: ManageErrorInputs) => {
  if (!errResp) {
    uiInform && uiInform('Erreur inconnue (pas de données fournies), merci de prévenir le support', 'error')
    return
  }

  let errMsg
  if (errResp && errResp.data.code === 404) {
    errMsg = 'URL incorrecte, merci de prévenir le support'
  } else if (errResp.data) {
    errMsg = `Echec: ${Object.entries(errResp.data)
      .map(([type, info]) => `(${type}) ${info}`)
      .join('\n')}`
  } else {
    errMsg = 'Erreur inconnue, merci de prévenir le support'
  }

  uiInform && uiInform(errMsg, 'error')
}

export const howManyUnfilledTasksInMarkdown = (txt?: string): number => {
  return txt ? txt.split('- [ ]').length - 1 : 0
}

export const howManyFilledTasksInMarkdown = (txt: string) => {
  return txt ? txt.split('[x]').length - 1 : 0
}

// TODO to be upgraded, this is personal estimation
export const estimateNbLines = (strLength: number, viewWidth: number, fontSize: number) => {
  return 1 + Math.floor(strLength / (70 * (viewWidth / 200) * (6 / fontSize)))
}

export const screenIsSmall = (context: React.FC) => {
  const theme = useTheme.bind(context)()
  return useMediaQuery.bind(context)(theme.breakpoints.down('xs'))
}

export const displayName = (p?: Patient | string | null): string => {
  if (!p || typeof p === 'string') return ''
  return `${p.firstName} ${p.lastName}` // ${p.sex ? ` (${p.sex})` : ''}`
}

export const displayUser = (u?: User): string => {
  if (!u) return ''
  return `${u.firstName} ${u.lastName}${u.title ? ` (${u.title})` : ''}`
}

export function getUpdated<T extends object>(origin: T, edited: T): Partial<T> {
  return _.reduce(
    _.entries(origin),
    (result, [key, value]) => {
      const editedVal = _.propertyOf(edited)(key)
      return _.isEqual(value, editedVal) || editedVal === undefined ? result : { ...result, [key]: editedVal }
    },
    {}
  )
}

export const jsonParseOrNull = (txt: string): object | null => {
  try {
    return JSON.parse(txt)
  } catch (e) {
    return null
  }
}

export const progressBetweenColors = (colorA: string, colorB: string, progression?: number): string => {
  const retHsl = convert.hex.hsl(colorA)
  const a = retHsl[0]
  const b = convert.hex.hsl(colorB)[0]
  const p = progression ?? 0.5

  const shorterDist = Math.min((b - a) % 360, (a - b) % 360) * p
  const direction = (b - a) % 360 < (a - b) % 360 ? 1 : -1

  return `#${convert.hsl.hex([a + direction * shorterDist, retHsl[1], retHsl[2]])}`
}

export const isAccessValid = (access?: Access): boolean => {
  if (!access) return false
  return (
    access.status === 'validated' &&
    new Date(access.startDatetime).valueOf() < _.now().valueOf() &&
    new Date(access.endDatetime).valueOf() > _.now().valueOf()
  )
}
/**
 * Returns a function that tests its string argument on RegExp expression re provided
 * @param re
 */
export const reTest = (re: RegExp): ((s: string) => boolean) => RegExp('').test.bind(re)
