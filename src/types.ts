export type BackListResponse<T> = {
  results: T[]
  count: number
  next?: string
}

export type BackValidationError = { [Key in string]: string }

export interface BaseApiModel {
  uuid: string
  createdAt?: string
  updatedAt?: string
  deletedAt?: string
}

export type FrontInput<Type> = Omit<Type, 'uuid'> & { uuid?: string }

export type BasicFilter<Type> = Partial<Type> & {
  ordering?: string // key1, -key2
  limit?: number
  offset?: number
  search?: string
}

export type State<Type> = Type & {
  loading?: boolean
}

export const baseApiModelSelectId = ({ uuid: id }: any): string => id ?? ''

export type Backend = {
  version?: null | User
  canManageAccounts?: boolean
  demoUsername?: string
}

export type RefreshResult = {
  access: string
  refresh: string
}

export type LoginResult = {
  user: User
  accessToken: string
  refreshToken: string
}

export type User = BaseApiModel & {
  username?: string
  email?: string
  title?: string
  firstName?: string
  lastName?: string
  authorizedReanimationServices?: string[]
  personalNotes?: string
  lastEditedPersonalNotes?: string
}

export type IntensiveCareService = BaseApiModel & {
  hospital: string
  hospitalName: string
  name: string
  users?: string[]
  units: string[]
}

type AccessStatus = 'created' | 'validated' | 'denied'

export type Access = BaseApiModel & {
  intensiveCareService: string
  user: string
  startDatetime: string
  endDatetime: string
  status: AccessStatus
  reviewedBy: string
  rightEdit: boolean
  rightReview: boolean
}

export type Unit = BaseApiModel & {
  intensiveCareService: string
  name: string
  beds: string[]
}

export type Bed = BaseApiModel & {
  unit: string
  usable?: boolean
  unitIndex: number
  currentPatientStay: string | null
  bedDescription: string
}

// export type BasicStay = BaseApiModel & {
//   patient: Patient | string | null
//   todoList?: PatientNote
//   failureMeasures?: PatientFailure[]
//   detections?: PatientDetection[]
//   hospitalisationCause?: string
//   severity?: StaySeverity
//   startDate?: string
//   endDate?: string | null
// }

export type SeverityType = 0 | 1 | 2

export type Stay = BaseApiModel & {
  bed: string
  patient: Patient | string | null
  startDate?: string
  endDate?: string | null
  hospitalisationCause?: string
  severity: SeverityType
  // read_only
  createdBy?: string
  isFinished?: boolean
  bedDescription?: string
  todoList?: PatientNote
  notes?: PatientNote[]
  treatmentLimitations?: TreatmentLimitation[]
  detections?: PatientDetection[]
  failureMeasures?: PatientFailure[]
}

export type PatientSex = 'm' | 'w' | 'u'

export type Patient = BaseApiModel & {
  localId: string
  firstName: string
  lastName: string
  sex: PatientSex
  birthDate: string
  sizeCm: number | null
  weightKg: number | null
  antecedents: { [Key: string]: string }
  allergies: { [Key: string]: string }
  bed?: string
  currentStay: string | null
}

export type StatusMeasure = BaseApiModel & {
  name: string
  id: number | string
  value: string
  patient: number | string
  statusType: number
  createdDate: string
  createdBy: string
  reanimationService: string
  idUnitStay: string
}

type Measure = BaseApiModel & {
  stay: string
  measureDatetime: string
  createdBy?: string
}

export type TreatmentLimitation = Measure & {
  noAcrReanimation?: boolean
  noNewFailuresOrTherapRaiseTreatment?: boolean
  noCatecholamines?: boolean
  noIntubation?: boolean
  noAssistedVentilation?: boolean
  noO2?: boolean
  noEer?: boolean
  noTransfusion?: boolean
  noSurgery?: boolean
  noPICOrDVE?: boolean
  noNewAntibiotherapy?: boolean
  noSirurgicalReintervetion?: boolean
  noNewComplementaryExams?: boolean
  noBiologicalResults?: boolean
  pressorAminesStop?: boolean
  eerStop?: boolean
  fio221percent?: boolean
  oxygenotherapyStop?: boolean
  mecanicVentilationStop?: boolean
  extubation?: boolean
  nutritionStop?: boolean
  hydratationStop?: boolean
  antibioticStop?: boolean
  dveAblation?: boolean
  ecmoStop?: boolean
  allCurrentTherapeuticsStop?: boolean
  otherStops?: string
  fio2Limit?: number
  noMecanicVentilationMarkup?: boolean
  aminesLimitation?: number
  noReanimationAdmittance?: boolean
  treatmentLimitationsComments?: string
}

export type PatientDetection = Measure & {
  blse: boolean
  sarm: boolean
  bhc: boolean
  clostridium: boolean
  // [Key in string]: boolean
}

export type FailureType = 'lung' | 'kidney' | 'heart' | 'metabolic' | 'brain' | 'liver' | 'hematologic' | 'detection'

export type PatientFailure = Measure & {
  active: boolean
  indication: string
  failureType: FailureType
}

export type PatientNoteType = 'rdh' | 'evolution' | 'todo' | 'tl' | 'dn' | 'general'

export type PatientNote = BaseApiModel & {
  createdBy?: string
  stay?: string
  type: PatientNoteType
  content: string
}

export type IssueLabel =
  | '0-Request'
  | '1-Back'
  | '2-Front'
  | 'A-To do'
  | 'B-Doing'
  | 'C-Code review'
  | 'D-Qualification'
  | 'E-Finished'
  | 'X-Idea'
  | 'Y-Validated'
  | 'Z-Defined'
  | 'i-UX'
  | 'j-User request'
  | 'k-Technical'

export type PostedIssue = {
  title: string
  description: string
  isIncident: boolean
  label?: IssueLabel
  attachment: FileList
}

export type Issue = {
  title: string
  id: number
  iid: number
  projectId: number
  description: string
  state: 'opened' | 'closed'
  createdAt: string
  updatedAt: string
  closedAt: string
  closedBy: string
  labels: IssueLabel[]
  type: string
  issueType: string
  webUrl: string
  votes: string[]
  sp: number
}
