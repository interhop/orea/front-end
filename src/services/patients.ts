import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackValidationError, Patient } from 'types'

export interface IServicesPatients {
  updatePatient: (
    id: string,
    data: Partial<Patient>
  ) => Promise<AxiosResponse<Patient> | AxiosError<BackValidationError>>
  createPatient: (data: Partial<Patient>) => Promise<AxiosResponse<Patient> | AxiosError<BackValidationError>>
}

const servicesPatients: IServicesPatients = {
  updatePatient: (id, data) => {
    const url = `${config.apiPath.patients}/${id}`
    console.log(`Updating patient at: ${url}`, data)
    return api.patch(url, data)
  },
  createPatient: (data) => {
    console.log(`Creating patient at: ${config.apiPath.patients}`, data)
    return api.post(config.apiPath.patients, data)
  }
}

export default servicesPatients

/*
export const submitDetectionChecklist = (nbWeeks, patientId) => {
  return (detections, thenCb, catchCb) => {
    let detections_ER_weekly = [...Array(nbWeeks).keys()].map(() => false)
    let detections_orl_weekly = [...Array(nbWeeks).keys()].map(() => false)
    Object.entries(detections).forEach(([k, v]) => {
      if (k.startsWith('detections_orl_weekly_')) detections_orl_weekly[k.split('_').pop() - 1] = v
      if (k.startsWith('detections_ER_weekly_')) detections_ER_weekly[k.split('_').pop() - 1] = v
    })

    let data = {
      detection_covid: detections.detection_covid,
      detection_orl_entree: detections.detection_orl_entree,
      detection_ER_entree: detections.detection_ER_entree,
      detections_ER_weekly: detections_ER_weekly,
      detections_orl_weekly: detections_orl_weekly
    }
    const url = `${config.path.patient}${patientId}/`
    console.log(`Sending to: ${url}`, data)

    if (DEV_MODE) {
      thenCb({ data: data })
      return
    }

    axios({
      method: 'patch',
      url,
      data: data,
      ...config.axios,
      headers: {
        'Content-Type': 'Application/json',
        'Access-Control-Allow-Origin': '*'
      }
    })
      .then(thenCb)
      .catch(catchCb)
  }
}
*/

/*
export const getDemographicData = (patientData) => {
  const {
    first_name,
    family_name,
    birth_date,
    weight_kg,
    size_cm,
    NIP_id,
    sex,
    current_unit_stay,
    hospitalisation_cause
  } = patientData
  const temData = {
    first_name,
    family_name,
    birth_date,
    weight_kg,
    size_cm,
    NIP_id,
    sex,
    current_unit_stay,
    hospitalisation_cause
  }

  const { unit_stays } = patientData
  if (unit_stays && unit_stays.length) {
    temData.hospitalisationDate = new Date(Math.min(...unit_stays.map((s) => new Date(s.start_date))))
  }
  return _.cloneDeep(temData)
}

export const getSeverity = (fullData) => ({ value: fullData.severity })

export const getDetectionData = (patientData) => {
  const { status_measures, current_unit_stay } = patientData

  let detections = status_measures
    .filter((sm) => sm.status_type === smTypes.er_orl_detection.dbValue)
    .filter((sm) => new Date(sm.created_date) >= new Date(current_unit_stay.start_date))

  const getMostRecentDate = (list) =>
    list.length > 0 && list.sort((a, b) => new Date(a.created_date) <= new Date(b.created_date))[0].created_date

  let lastNegDate = getMostRecentDate(detections.filter((sm) => !sm.value))
  let lastBLSEDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('BLSE') !== -1))
  let lastSARMDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('SARM') !== -1))
  let lastBHCDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('BHC') !== -1))
  let lastClostridiumDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('Clostridium') !== -1))
  let sinceLastDetection = detections.length && nbDaysBetween(getMostRecentDate(detections), new Date())

  return {
    detections,
    sinceLastDetection,
    lastNegDate,
    lastBLSEDate,
    lastSARMDate,
    lastBHCDate,
    lastClostridiumDate
  }
}

export const completeDetectionsWithNewMeasures = (newMeasure, detectionsData) => {
  let detections = _.cloneDeep(detectionsData.detections)
  detections.push(newMeasure)

  const getMostRecentDate = (list) =>
    list.length && list.sort((a, b) => new Date(a.created_date) <= new Date(b.created_date))[0].created_date

  let lastNegDate = getMostRecentDate(detections.filter((sm) => !sm.value))
  let lastBLSEDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('BLSE') !== -1))
  let lastSARMDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('SARM') !== -1))
  let lastBHCDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('BHC') !== -1))
  let lastClostridiumDate = getMostRecentDate(detections.filter((sm) => sm.value.indexOf('Clostridium') !== -1))
  let sinceLastDetection = detections.length && nbDaysBetween(getMostRecentDate(detections), new Date())

  return {
    detections,
    sinceLastDetection,
    lastNegDate,
    lastBLSEDate,
    lastSARMDate,
    lastBHCDate,
    lastClostridiumDate
  }
}

export const getAntecedentsData = (fullData) => {
  let temList
  if (fullData.antecedents) {
    try {
      let temJson = JSON.parse(fullData.antecedents)
      temList = Object.entries(temJson).map(([k, v]) => ({
        title: k.split('#')[0], // 'cardio#0', 'cardio#1', etc.
        value: v
      }))
    } catch (e) {
      temList = []
    }
  } else {
    temList = []
  }

  let antecedentsIsEmpty = temList.length === 0 || (temList.length === 1 && temList[0].title === 'Inconnus')
  return { listItems: temList, isEmpty: antecedentsIsEmpty }
}

export const getAllergiesData = (fullData) => {
  let temList
  if (fullData.allergies) {
    try {
      temList = JSON.parse(fullData.allergies)
    } catch (e) {
      temList = []
    }
  } else {
    temList = []
  }

  let allergiesIsEmpty = temList.length === 0 || (temList.length === 1 && temList[0] === 'Inconnues')
  return { listItems: temList, isEmpty: allergiesIsEmpty }
}

export const getTextData = (field) => (fullData) => {
  let temDate = fullData[`last_edited_${field}`]
  return {
    text: fullData[field],
    lastEdited: temDate ? new Date(temDate) : null
  }
}

export const getFailuresData = (fullData) => {
  const {
    heart_failure,
    bio_chemical_failure,
    brain_failure,
    lung_failure,
    kidney_failure,
    liver_failure,
    hematologic_failure
  } = fullData

  const temData = {
    heart_failure,
    bio_chemical_failure,
    brain_failure,
    lung_failure,
    kidney_failure,
    liver_failure,
    hematologic_failure
  }

  return temData
}

export const getStatusMeasuresData = (fullData) => {
  const { status_measures, unit_stays, current_unit_stay } = fullData
  let temData = { measures: status_measures }
  if (unit_stays && unit_stays.length) {
    temData.hospitalisationDate = new Date(Math.min(...unit_stays.map((s) => new Date(s.start_date))))
    temData.hospitalisationEndDate = unit_stays.filter((s) => !s.is_finished).length
      ? null
      : new Date(Math.max(...unit_stays.map((s) => new Date(s.end_date))))
  } else if (current_unit_stay) {
    temData.hospitalisationDate = current_unit_stay.start_date
    temData.hospitalisationEndDate = current_unit_stay.end_date ? new Date(current_unit_stay.end_date) : null
  }
  if (fullData.weight_kg) {
    temData.weight_kg = fullData.weight_kg
  }
  return temData
}

export const completeStatusMeasuresData = (newStatusMeasures, currentData) => {
  currentData.measures.splice(-1, 0, ...newStatusMeasures)
  return _.cloneDeep(currentData)
}

export const getTreatmentLimitations = (fullData) => {
  const temData = _.pick(fullData, [
    'no_acr_reanimation',
    'no_new_failures_or_therap_raise_treatment',
    'no_catecholamines',
    'no_intubation',
    'no_assisted_ventilation',
    'no_o2',
    'no_eer',
    'no_transfusion',
    'no_surgery',
    'no_PIC_or_DVE',
    'no_new_antibiotherapy',
    'no_sirurgical_reintervetion',
    'no_new_complementary_exams',
    'no_biological_results',
    'pressor_amines_stop',
    'eer_stop',
    'fio2_21percent',
    'oxygenotherapy_stop',
    'mecanic_ventilation_stop',
    'extubation',
    'nutrition_stop',
    'hydratation_stop',
    'antibiotic_stop',
    'dve_ablation',
    'ecmo_stop',
    'all_current_therapeutics_stop',
    'other_stops',
    'fio2_limit',
    'no_mecanic_ventilation_markup',
    'amines_limitation',
    'no_reanimation_admittance',
    'treatment_limitations_comments'
  ])
  temData.lastEdited = fullData.last_edited_treatment_limitations
  return _.cloneDeep(temData)
}

/!**
 *
 * @param {Patient} patient
 * @param {UnitStay} current_unit_stay
 *!/
export const getLastDetection = (patient, current_unit_stay) => {
  const detectionCode = smTypes.er_orl_detection.dbValue

  if (!current_unit_stay && !patient.current_unit_stay) return null
  current_unit_stay = current_unit_stay || patient.current_unit_stay

  if (!patient) return null
  if (!patient.status_measures || patient.status_measures.length === 0) return null
  const currentStayDetections = patient.status_measures
    .filter((sm) => Number(sm.status_type) === Number(detectionCode))
    .filter((sm) => {
      // true if detection was done during current_stay
      if (sm.id_unit_stay && sm.id_unit_stay.toString() === current_unit_stay.id.toString()) return true
      const cd = new Date(sm.created_date)

      return (
        cd > new Date(current_unit_stay.start_date) &&
        (!current_unit_stay.end_date || cd < new Date(current_unit_stay.end_date))
      )
    })

  return currentStayDetections.sort((smA, smB) => new Date(smB.created_date) - new Date(smA.created_date))[0]
}
*/
