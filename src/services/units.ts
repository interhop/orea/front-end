import { AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackListResponse, Unit } from 'types'

export interface IServicesUnits {
  fetchUnits: (params?: Partial<Unit>) => Promise<AxiosResponse<BackListResponse<Unit>>>
}

const servicesUnits: IServicesUnits = {
  fetchUnits: (params) => {
    console.log(`Fetching units: ${config.apiPath.units}`, params)
    return api.get(config.apiPath.units, { params })
  }
}

export default servicesUnits
