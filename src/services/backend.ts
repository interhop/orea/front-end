import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { Backend, BackValidationError } from 'types'

export interface IServicesBackend {
  /**
   * Function to get backend state
   */
  retrieveState: () => Promise<AxiosResponse<Backend> | AxiosError<BackValidationError>>
}

const servicesBackend: IServicesBackend = {
  retrieveState: () => {
    console.log(`Retrieving backend state: ${config.apiPath.backendState}`)
    return api.get(config.apiPath.backendState)
  }
}

export default servicesBackend
