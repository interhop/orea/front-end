import { Dictionary } from '@reduxjs/toolkit'
import { AxiosResponse } from 'axios'
import _ from 'lodash'

import api from './api'
import config from 'config'
import { Bed, IntensiveCareService, PatientNote, Patient, Unit, BackListResponse, FrontInput, Stay } from 'types'

export interface IServicesIntensiveCareServices {
  fetchNewIntensiveCareService: (accessCode: string) => Promise<any>
  fetchIntensiveCareService: (id: string) => Promise<any>
  fetchIntensiveCareServices: (
    params?: Partial<IntensiveCareService>
  ) => Promise<AxiosResponse<BackListResponse<IntensiveCareService>>>
  subscribeIntensiveCareService: (id: string, accessCode: string) => Promise<any>
  unsubscribeIntensiveCareService: (id: string) => Promise<void>
  // submitRemoveIntensiveCareService: (serviceId: string) => Promise<any>
}

const servicesIntensiveCareServices: IServicesIntensiveCareServices = {
  fetchNewIntensiveCareService: (accessCode) => {
    console.log(`Sending to: ${config.apiPath.intensiveCareServices}`, accessCode)
    return api.get(config.apiPath.intensiveCareServices, {
      params: {
        reanimation_service_code: accessCode
      }
    })
  },
  fetchIntensiveCareService: (id) => {
    const url = `${config.apiPath.intensiveCareServices}/${id}`
    console.log(`Fetching int-care service: ${url}`)
    return api.get(config.apiPath.intensiveCareServices)
  },
  fetchIntensiveCareServices: (params) => {
    console.log(`Fetching int-care services: ${config.apiPath.intensiveCareServices}`, params)
    return api.get(config.apiPath.intensiveCareServices, { params })
  },
  subscribeIntensiveCareService: (id: string, accessCode: string) => {
    const url = `${config.apiPath.intensiveCareServices}/${id}/subscribe`
    console.log(`Subscribing to a int-care service: ${url}`)
    return api.patch(url, { access_code: accessCode })
  },
  unsubscribeIntensiveCareService: (id: string) => {
    const url = `${config.apiPath.intensiveCareServices}/${id}/unsubscribe`
    console.log(`Unsubscribing to a int-care service: ${url}`)
    return api.patch(url)
  }
}

export default servicesIntensiveCareServices

const basicBedInfo = (bed: Bed, stay: Stay) => {
  const patient = stay.patient as Patient
  return !patient ? '' : `${bed.unitIndex} - ${patient.firstName} ${patient.lastName}`
}

const buildBedTodoListMarkdownSummary = (bed: Bed, stay: Stay): string => {
  if (!bed.currentPatientStay) return ''

  const toBeDone = stay.todoList?.content
    .split(`\n`)
    // TODO noticed a \r at each line, when TodoList updated through TodoList dialog
    .filter((r: string) => /^- \[ ].*$/.test(r.replace(/\\r/g, '').trim()))
    .join(`\n`)
  if (!toBeDone) return ''

  return `#### ${basicBedInfo(bed, stay)}\n${toBeDone}\n`
}

export const buildUnitTodoListSummary = (beds: Bed[], stays: Dictionary<Stay>): FrontInput<PatientNote> => {
  let res = ''
  let updatedAt: string | undefined
  beds.forEach((bed) => {
    if (!bed.currentPatientStay) return ''
    const stay = stays[bed.currentPatientStay]
    if (!stay) return ''
    const patientLastEdited = stay.todoList?.updatedAt
    if (!updatedAt || (patientLastEdited && new Date(patientLastEdited) > new Date(updatedAt)))
      updatedAt = patientLastEdited

    res = `${res}${buildBedTodoListMarkdownSummary(bed, stay)}`
  })
  return {
    content: res,
    updatedAt: updatedAt,
    type: 'todo'
  }
}

export const buildICServiceTodoListSummary = (
  units: Unit[],
  beds: Dictionary<Bed>,
  stays: Dictionary<Stay>
): FrontInput<PatientNote> => {
  let res = ''
  let updatedAt: string | undefined
  units.forEach((unit) => {
    const unitBeds = _.filter(_.map(unit.beds, _.propertyOf(beds))) as Bed[]

    const unitSummary = buildUnitTodoListSummary(unitBeds, stays)
    if (!updatedAt || (unitSummary.updatedAt && new Date(unitSummary.updatedAt) > new Date(updatedAt)))
      updatedAt = unitSummary.updatedAt

    res = `${res}## ${unit.name}\n${unitSummary.content}`
  })
  return {
    content: res,
    updatedAt: updatedAt,
    type: 'todo'
  }
}

// export const submitRemoveIntensiveCareService = () => {
//   return (serviceId, thenCb, catchCb) => {
//     const dataToSend = {
//       action: 'remove'
//     }
//
//     const url = `${config.apiPath.intensiveCareServices}${serviceId}/`
//     console.log(`Sending to: ${url}`, dataToSend)
//
//     if (DEV_MODE) {
//       thenCb({ data: { removed: serviceId } })
//       return
//     }
//
//     api({
//       method: 'patch',
//       url,
//       data: dataToSend,
//       ...config.api,
//       headers: {
//         'Content-Type': 'application/json',
//         'Access-Control-Allow-Origin': '*'
//       }
//     })
//       .then(thenCb)
//       .catch(catchCb)
//   }
// }
