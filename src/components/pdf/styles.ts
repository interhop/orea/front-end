import { styled } from '@mui/material/styles'
import { Font, StyleSheet } from '@react-pdf/renderer'

import regFont from 'assets/fonts/SpaceGrotesk-VariableFont_wght.ttf'
import boldFont from 'assets/fonts/SpaceGrotesk-Bold.ttf'
import lightFont from 'assets/fonts/SpaceGrotesk-Light.ttf'
import italicRobotoFont from 'assets/fonts/Roboto-Italic.ttf'

Font.register({
  family: 'SpaceGrotesk',
  fonts: [
    {
      src: regFont
    },
    {
      src: boldFont,
      fontWeight: 'bold'
    },
    {
      src: lightFont,
      fontWeight: 'normal',
      fontStyle: 'italic'
    }
  ]
})

Font.register({
  family: 'Roboto',
  fonts: [
    {
      src: italicRobotoFont,
      fontWeight: 'normal',
      fontStyle: 'italic'
    }
  ]
})

export const pdfStyles = (fontSize = 10) =>
  StyleSheet.create({
    pageTitle: {
      width: '100%',
      alignItems: 'center',
      fontSize: fontSize,
      marginTop: fontSize * 1.5,
      top: 2
    },
    subTitle: {
      marginTop: fontSize / 2,
      marginBottom: fontSize / 6
    },
    footer: {
      fontSize: fontSize / 2,
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'absolute',
      bottom: 2
    },
    bold: {
      fontFamily: 'SpaceGrotesk',
      fontWeight: 'bold',
      marginRight: fontSize / 2
    },
    italic: {
      fontFamily: 'Roboto',
      fontStyle: 'italic'
    },
    pdfFlexRow: {
      flexDirection: 'row'
    },
    additionalInfo: {
      marginLeft: fontSize / 2
    },
    pdfText: {
      fontFamily: 'SpaceGrotesk',
      fontSize: fontSize
    },
    icon: {
      width: fontSize * 2,
      height: fontSize * 2,
      color: 'black'
    },
    component: {
      padding: 1,
      margin: 1,
      border: 0.5
    },
    components: {
      display: 'flex',
      flexWrap: 'wrap'
    }
  })

const PREFIX = 'Pdf'
export const classes = {
  buttonProgress: `${PREFIX}-buttonProgress`,
  iconButton: `${PREFIX}-iconButton`,
  icon: `${PREFIX}-icon`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.buttonProgress}`]: {
    color: theme.palette.secondary.contrastText,
    fontSize: 10
  },
  [`& .${classes.iconButton}`]: {
    padding: theme.spacing(4, 6, 4, 4),
    width: '100%',
    height: '100%'
  },
  [`& .${classes.icon}`]: {
    width: '100%',
    height: '100%',
    margin: '2px',
    color: theme.palette.secondary.contrastText
  }
}))
