import React, { useEffect, useState } from 'react'
import { Buffer } from 'buffer'
import { Dictionary } from '@reduxjs/toolkit'
import _ from 'lodash'

import { CircularProgress, SvgIconProps, Tooltip } from '@mui/material'
import { Document, usePDF, PDFViewer } from '@react-pdf/renderer'

import { classes, Root } from './styles'
import PdfService, { PdfServiceData } from './PdfService'
import DownloadDocIcon from 'assets/icons/download_doc'
import { dateTimeToFileName } from 'assets/utils'
import { useAppSelector } from 'state'
import { PrintSettings } from 'state/settings'
import { Bed, IntensiveCareService, Stay, Unit, User } from 'types'

const fullDataForPDf = (
  service: IntensiveCareService,
  units: Dictionary<Unit>,
  beds: Dictionary<Bed>,
  stays: Dictionary<Stay>
): PdfServiceData => {
  return {
    name: service.name,
    hospitalName: service.hospitalName,
    units: service.units.map((uId) => ({
      name: (units[uId] as Unit).name,
      beds: units[uId]
        ? (units[uId] as Unit).beds
            .filter((bId) => !!beds[bId]?.currentPatientStay)
            .map((bId) => ({
              uuid: bId,
              unitIndex: (beds[bId] as Bed).unitIndex,
              currentStay: stays[beds[bId]?.currentPatientStay || ''] as Stay
            }))
        : []
    }))
  }
}

type DocProps = {
  service: IntensiveCareService
  units: Dictionary<Unit>
  beds: Dictionary<Bed>
  stays: Dictionary<Stay>
  settings?: PrintSettings
  me: User
  selectedBeds: string[]
}

const PdfRenderer: React.FC<DocProps> = ({ service, units, beds, stays, settings, me, selectedBeds }) => {
  const docName = `Service-${service?.name}-${dateTimeToFileName(_.now()).replace(/\./g, '')}`
  const fileName = `${docName}.pdf`

  const [instance, updateInstance] = usePDF({
    document: (
      <Document title={docName} author="Orea" subject="Info du patient" keywords={`beds;Orea;${service?.name}`}>
        <PdfService
          service={fullDataForPDf(service, units, beds, stays)}
          user={me}
          selectedBeds={selectedBeds}
          settings={settings}
        />
      </Document>
    )
  })

  const dataKey = new Buffer(
    JSON.stringify({
      settings,
      selectedBeds
      // beds: beds.entities,
      // units: units.entities,
      // stays: stays.entities,
      // me: me.user
    })
  ).toString('base64')

  useEffect(() => {
    updateInstance()
  }, [dataKey, units.status, beds.status, stays.status])
  // }, [dataKey])

  return !instance.loading ? (
    instance.error || !instance.url ? (
      <>Error {instance.error}</>
    ) : (
      <a href={instance.url} download={fileName}>
        <Tooltip title="Télécharger le pdf" arrow>
          <div className={classes.iconButton}>
            <DownloadDocIcon className={classes.icon} />
          </div>
        </Tooltip>
      </a>
    )
  ) : (
    <CircularProgress size={24} className={classes.buttonProgress} />
  )
}

export type DownloadLinkProps = SvgIconProps & {
  service?: IntensiveCareService
}

export const DownloadLink: React.FC<DownloadLinkProps> = ({ service }) => {
  const { settings, selection, beds, units, stays, me } = useAppSelector((state) => state)

  if (!service) return <></>
  if (!me.user) return <></>

  return (
    <Root>
      {units.status !== 'loading' && beds.status !== 'loading' && stays.status !== 'loading' ? (
        <PdfRenderer
          service={service}
          units={units.entities}
          beds={beds.entities}
          stays={stays.entities}
          me={me.user}
          selectedBeds={_.cloneDeep(selection.beds)}
          settings={settings.printSettings}
        />
      ) : (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </Root>
  )
}

export const DebugDisplay: React.FC<DownloadLinkProps> = ({ service }) => {
  const { settings, selection, beds, units, stays, me } = useAppSelector((state) => state)
  const [pdfData, setPdfData] = useState<PdfServiceData | null>(null)

  const dataKey = new Buffer(
    JSON.stringify({
      settings: settings.printSettings,
      selection: selection.beds
      // beds: beds.entities,
      // units: units.entities,
      // stays: stays.entities,
      // me: me.user
    })
  ).toString('base64')

  useEffect(() => {
    if (!service || !me.user || units.status === 'loading' || beds.status === 'loading' || stays.status === 'loading')
      return
    setPdfData(fullDataForPDf(service, units.entities, beds.entities, stays.entities))
  }, [dataKey, units.status, beds.status, stays.status])

  if (!service) return <></>
  if (!me.user) return <></>

  const docName = `Service-${service?.name}-${dateTimeToFileName(_.now()).replace(/\./g, '')}`

  return (
    <Root style={{ width: '100%', zIndex: 100 }}>
      {units.status !== 'loading' && beds.status !== 'loading' && stays.status !== 'loading' ? (
        <PDFViewer width="100%" height="100%">
          <Document title={docName} author="Orea" subject="Info du patient" keywords={`beds;Orea;${service?.name}`}>
            {pdfData && (
              <PdfService
                service={pdfData}
                user={me.user}
                selectedBeds={selection.beds}
                settings={settings.printSettings}
              />
            )}
          </Document>
        </PDFViewer>
      ) : (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </Root>
  )
}

export default DownloadLink
