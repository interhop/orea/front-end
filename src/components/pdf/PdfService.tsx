import React from 'react'
import _ from 'lodash'

import { Page, Text, View } from '@react-pdf/renderer'

import PdfStay from './PdfStay'
import { pdfStyles } from './styles'
import { dateTimeToStr, displayName } from 'assets/utils'
import { PrintSettings } from 'state/settings'
import { User, Stay } from 'types'

type PdfUnitBedProps = {
  bedData: {
    uuid: string
    unitIndex: number
    currentStay: Stay
  }
  user: User | null
  fontSize: number
  settings?: PrintSettings
}

export const PdfUnitBed: React.FC<PdfUnitBedProps> = ({ bedData, user, fontSize, settings }) => {
  const styles = pdfStyles(fontSize)

  return (
    <View wrap={false} style={{ flexGrow: 1, maxWidth: settings?.onePatientPerPage ? 590 : 290 }}>
      <Text style={[styles.pdfText, styles.bold]}>{`${bedData.unitIndex} - ${displayName(
        bedData.currentStay.patient
      )}`}</Text>
      <PdfStay
        data={bedData.currentStay}
        user={user}
        settings={settings}
        withSituation={false}
        fontSize={fontSize * 0.8}
      />
    </View>
  )
}

type PdfReanimationUnitProps = {
  unitData: {
    name: string
    beds: {
      uuid: string
      unitIndex: number
      currentStay: Stay
    }[]
  }
  user: User | null
  fontSize: number
  settings?: PrintSettings
  newPage: boolean
}

const PdfReanimationUnit: React.FC<PdfReanimationUnitProps> = ({ unitData, user, fontSize, settings, newPage }) => {
  const bedElements = unitData.beds
    .sort((a, b) => a.unitIndex - b.unitIndex)
    .map((bed) => (
      <PdfUnitBed
        key={`pdfunitbed-${bed.unitIndex}`}
        bedData={bed}
        user={user}
        settings={settings}
        fontSize={fontSize * 0.8}
      />
    ))
  return (
    <View break={newPage} style={{ marginLeft: '5px', marginRight: '5px' }}>
      <View style={{ width: '100%', alignItems: 'center' }}>
        <View
          style={{
            height: 0,
            width: '80%',
            borderTop: 1,
            borderTopWidth: 1,
            margin: '2px'
          }}
        />
        <View style={{ flexGrow: 1 }}>
          <Text style={{ fontSize: fontSize }}>Unité {unitData.name}</Text>
        </View>
      </View>
      {_.chunk(bedElements, settings?.onePatientPerPage ? 1 : 2).map((bedSet) => (
        <View key={`viewset-${bedSet[0].key}`}>
          <View style={{ width: '100%', alignItems: 'center' }}>
            <View
              style={{
                height: 0,
                width: '80%',
                borderTop: 1,
                borderTopWidth: 0.5,
                margin: '2px'
              }}
            />
          </View>

          <View
            style={{
              width: '100%',
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}
          >
            {bedSet.map((bedComponent, i) => (
              <React.Fragment key={`bed-fragment-${i}`}>
                {i !== 0 && (
                  <View
                    style={{
                      alignSelf: 'center',
                      width: 0,
                      height: '150px',
                      borderLeft: 1,
                      borderLeftWidth: 0.5,
                      margin: '2px'
                    }}
                  />
                )}
                {bedComponent}
              </React.Fragment>
            ))}
          </View>
        </View>
      ))}
    </View>
  )
}

// export type PdfServiceData = Omit<IntensiveCareService, 'units'> & {
//   units: (Omit<Unit, 'beds'> & { beds: (Omit<Bed, 'currentPatientStay'> & { currentPatientStay: Stay })[] })[]
// }

export type PdfServiceData = {
  name: string
  hospitalName: string
  units: {
    name: string
    beds: {
      uuid: string
      unitIndex: number
      currentStay: Stay
    }[]
  }[]
}

type PdfServiceProps = {
  service: PdfServiceData
  user: User | null
  selectedBeds: string[]
  settings?: PrintSettings
}

const PdfService: React.FC<PdfServiceProps> = ({ service, user, selectedBeds, settings }) => {
  const fontSize = settings?.onePatientPerPage ? 14 : 10

  const styles = pdfStyles(fontSize)

  return (
    <Page size="A4" wrap>
      <View fixed style={styles.pageTitle}>
        <Text render={() => `Service ${service.name.toUpperCase()}`} />
      </View>

      {service.units
        .filter((unit) => unit.beds.map((b) => b.uuid).some((id) => _.includes(selectedBeds, id)))
        .map((unit, i) => {
          return settings?.onePatientPerPage ? (
            <React.Fragment key={`unit-${i}`}>
              {unit.beds
                .filter((b) => _.includes(selectedBeds, b.uuid))
                .map((bed, j) => (
                  <PdfReanimationUnit
                    key={`unit-${i}-${j}`}
                    unitData={{ ...unit, beds: [bed] }}
                    user={user}
                    fontSize={fontSize}
                    settings={settings}
                    newPage={!!(i || j)}
                  />
                ))}
            </React.Fragment>
          ) : (
            <PdfReanimationUnit
              key={`unit-${i}`}
              unitData={{ ...unit, beds: unit.beds.filter((b) => _.includes(selectedBeds, b.uuid)) }}
              user={user}
              fontSize={fontSize}
              settings={settings}
              newPage={i > 0 && !!(settings?.newPageForUnit || settings?.onePatientPerPage)}
            />
          )
        })}

      <View fixed style={styles.footer}>
        <Text render={() => `${dateTimeToStr(new Date())}`} />
        <Text
          render={
            ({ pageNumber }) => `${pageNumber}` // / ${totalPages}`
          }
        />
        <Text>Hôpital {service.hospitalName}</Text>
      </View>
    </Page>
  )
}

export default PdfService
