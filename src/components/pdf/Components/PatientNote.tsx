import React from 'react'
import _ from 'lodash'

import Markdown, { Options } from 'react-markdown'
import { Link, Text, View } from '@react-pdf/renderer'

import { pdfStyles } from './pdfStyles'
import { dateTimeToStr, estimateNbLines, reTest } from 'assets/utils'
import { PatientNote } from 'types'
import { LiProps, OrderedListProps, UnorderedListProps } from 'react-markdown/lib/ast-to-react'

export const patientNoteEstimatedNbLines = (
  note: PatientNote | undefined,
  maxWidth: number,
  fontSize: number,
  isCheckList = false
) => {
  if (!note) return 0
  const text = !isCheckList
    ? note.content
    : note.content
        .split(`\n`)
        .filter((r) => !/- \[x].*/.test(r))
        .join(`\n`)

  const lines = text.split(`\n`)
  if (lines.length === 1 && lines[0].length === 0) return 0
  return lines.map((line) => estimateNbLines(line.length, maxWidth, fontSize)).reduce((a, b) => a + b, 0) + 2 // + 2 for title and 'last edited' line
}

type PdfPatientNoteProps = {
  title: string
  note?: PatientNote
  isChecklist?: boolean
  fontSize: number
  maxWidth: number
  maxLines?: number
}

const PdfPatientNote: React.FC<PdfPatientNoteProps> = ({
  title,
  note,
  isChecklist = false,
  fontSize,
  maxWidth,
  maxLines
}) => {
  const styles = pdfStyles(fontSize)

  const formatContent = (): string => {
    if (!note) return 'Vide'
    if (isChecklist) {
      return _.reject(note.content.split(`\n`), reTest(/- \[x].*/)).join(`\n`)
    } else {
      return note.content.replace(/(\n)+/g, '\n').trim()
    }
  }

  const components: Options['components'] = {
    div: ({ children }) => <Text>{children}</Text>,
    span: ({ children }) => <Text>{children}</Text>,
    p: ({ children }) => <Text style={styles.pdfText}>{children}</Text>,
    b: ({ children }) => <Text style={[styles.pdfText, styles.bold]}>{children}</Text>,
    em: ({ children }) => <Text style={[styles.pdfText, styles.italic]}>{children}</Text>,
    i: ({ children }) => <Text style={[styles.pdfText, styles.italic]}>{children}</Text>,
    strong: ({ children }) => <Text style={[styles.pdfText, styles.bold]}>{children}</Text>,
    a: ({ children, href }) => (
      <Link style={[styles.pdfText, { textDecoration: 'underline' }]} src={href ?? ''}>
        {children}
      </Link>
    ),
    hr: ({ children }) => <View>{children}</View>,
    ol: ({ children }: OrderedListProps) => <View style={styles.pdfText}>{children}</View>,
    ul: ({ children }: UnorderedListProps) => <View style={styles.pdfText}>{children}</View>,
    li: ({ children }: LiProps) => <Text style={styles.pdfText}>• &nbsp;{children}</Text>,
    h1: ({ children }) => (
      <Text style={[styles.pdfText, { fontSize: fontSize + 2, fontWeight: 'bold' }]}>{children}</Text>
    ),
    h2: ({ children }) => <Text style={[styles.pdfText, { fontSize: fontSize + 2 }]}>{children}</Text>,
    h3: ({ children }) => <Text style={[styles.pdfText, { textDecoration: 'underline' }]}>{children}</Text>
  }

  return (
    <View style={{ maxWidth }}>
      <Text style={[styles.pdfText, styles.bold]}>{title}</Text>
      {note && <Text style={[styles.pdfText, styles.italic]}>Modifié le: {dateTimeToStr(note.updatedAt)}</Text>}

      <Markdown components={components}>{formatContent().split('\n').slice(0, maxLines).join('\n')}</Markdown>
    </View>
  )
}

export default PdfPatientNote
