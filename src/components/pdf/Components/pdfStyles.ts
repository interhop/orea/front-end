import { Font, StyleSheet } from '@react-pdf/renderer'

import regFont from 'assets/fonts/SpaceGrotesk-VariableFont_wght.ttf'
import boldFont from 'assets/fonts/SpaceGrotesk-Bold.ttf'
import lightFont from 'assets/fonts/SpaceGrotesk-Light.ttf'
import italicRobotoFont from 'assets/fonts/Roboto-Italic.ttf'

Font.register({
  family: 'SpaceGrotesk',
  fonts: [
    {
      src: regFont
    },
    {
      src: boldFont,
      fontWeight: 'bold'
    },
    {
      src: lightFont,
      fontWeight: 'normal',
      fontStyle: 'italic'
    }
  ]
})

Font.register({
  family: 'Roboto',
  fonts: [
    {
      src: italicRobotoFont,
      fontWeight: 'normal',
      fontStyle: 'italic'
    }
  ]
})

export const pdfStyles = (fontSize = 10) =>
  StyleSheet.create({
    pageTitle: {
      width: '100%',
      alignItems: 'center',
      fontSize: fontSize,
      marginTop: fontSize * 1.5,
      top: 1
    },
    subTitle: {
      marginTop: fontSize / 2,
      marginBottom: fontSize / 6
    },
    footer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'absolute',
      bottom: 5,
      left: 5,
      right: 5
    },
    footerText: {
      fontSize: fontSize / 2
    },
    bold: {
      fontFamily: 'SpaceGrotesk',
      fontWeight: 'bold',
      marginRight: fontSize / 2
    },
    italic: {
      fontFamily: 'Roboto',
      fontStyle: 'italic'
    },
    pdfFlexRow: {
      flexDirection: 'row'
    },
    additionalInfo: {
      marginLeft: fontSize / 2
    },
    pdfText: {
      fontFamily: 'SpaceGrotesk',
      fontSize: fontSize
    },
    icon: {
      width: fontSize * 2,
      height: fontSize * 2,
      color: 'black'
    },
    component: {
      padding: '1px',
      margin: '1px',
      border: 0.5
    },
    components: {
      display: 'flex',
      flexWrap: 'wrap'
    }
  })
