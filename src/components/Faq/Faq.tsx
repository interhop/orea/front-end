import React from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import Box from '@mui/material/Box'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import FormControl from '@mui/material/FormControl'
import Grid from '@mui/material/Unstable_Grid2'
import LoadingButton from '@mui/lab/LoadingButton'
import OutlinedInput from '@mui/material/OutlinedInput'
import Typography from '@mui/material/Typography'

import env from 'env'
import faq, { Question, Section } from 'faq'
import { classes, Root } from './styles'
import { createIssue } from 'state/issues'
import { AppDispatch } from 'state'

const EMAIL_QUESTION_MODEL = `mailto:${env.REACT_APP_GITLAB_MAILTO}
  ?subject=Question utilisateur
  &body=Votre question:%0D%0A'`

const filterQuestion =
  (search: string) =>
  (q: Question): boolean =>
    _.includes(`${q.subject}${q.answer}`.toLowerCase(), search.toLowerCase())

const filterFaq = (search: string, sections: Section[]): Section[] => {
  return sections
    .filter((s) => s.questions.some(filterQuestion(search)))
    .map(
      (s): Section => ({
        ...s,
        questions: s.questions.filter(filterQuestion(search))
      })
    )
}

type FaqProps = {
  loading?: boolean
}

const Faq: React.FC<FaqProps> = ({ loading }) => {
  const [search, setSearch] = React.useState<string>('')
  const [newQuestion, setNewQuestion] = React.useState<string>('')
  const dispatch = useDispatch<AppDispatch>()

  const onSubmit = () => {
    if (!newQuestion) return

    dispatch(
      createIssue({
        title: `Question: ${newQuestion}`,
        description: '',
        attachment: undefined,
        isIncident: false,
        label: '0-Request'
      })
    )
  }

  return (
    <Root>
      <Box className={classes.card}>
        <Typography variant="h4" className={classes.sectionTitle}>
          FAQ
        </Typography>

        <OutlinedInput
          key="faq-search"
          placeholder="Recherche"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          fullWidth
        />

        {(search ? filterFaq(search, faq) : faq).map((section, index) => (
          <div key={index} className={classes.section}>
            {section.subject && (
              <Typography variant="h5" className={classes.sectionTitle}>
                {section.subject}
              </Typography>
            )}
            {section.questions.map((q) => (
              <Accordion key={q.subject} defaultExpanded={false} className={classes.questionItem}>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.question}>
                  {q.subject}
                </AccordionSummary>
                <AccordionDetails className={classes.answer}>
                  <Typography variant="body1">{q.answer}</Typography>
                </AccordionDetails>
              </Accordion>
            ))}
          </div>
        ))}

        <form noValidate onSubmit={onSubmit}>
          <Grid container justifyContent="center" width="100%">
            <FormControl color="secondary" fullWidth variant="outlined" className={classes.input}>
              <Typography variant="body1">
                Autre question ? Posez-la ici (
                <a
                  onClick={() => window.open(EMAIL_QUESTION_MODEL, '_blank')}
                  rel="noreferrer"
                  className={classes.emailLink}
                >
                  ou envoyez-la par email
                </a>
                ) :
              </Typography>
              <OutlinedInput
                required
                fullWidth
                key="faq-ask"
                value={newQuestion}
                onChange={(e) => setNewQuestion(e.target.value)}
              />
            </FormControl>
            <LoadingButton
              loading={!!loading}
              type="submit"
              variant="contained"
              className={classes.sendButton}
              id="faq-ask-button-submit"
              disabled={!newQuestion}
            >
              Envoyer
            </LoadingButton>
          </Grid>
        </form>
      </Box>
    </Root>
  )
}

export default Faq
