import React from 'react'

import { FormProps } from '@rjsf/core'
import BuiForm from '@rjsf/mui'
import { RJSFSchema, Widget } from '@rjsf/utils'
import validator from '@rjsf/validator-ajv8'

import Dialog, { DialogProps } from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import OutlinedInput from '@mui/material/OutlinedInput'
import InputAdornment from '@mui/material/InputAdornment'
import IconButton from '@mui/material/IconButton'
import { Today } from '@mui/icons-material'

import { toISOLocale } from 'assets/utils'

export const MyBuiForm: React.FC<Omit<FormProps, 'validator'>> = (props) => {
  const CustomDateTimeWidget = (): Widget => (props_) =>
    (
      <FormControl color="secondary" fullWidth variant="outlined">
        <InputLabel>{props_.schema.title}</InputLabel>
        <OutlinedInput
          required
          id={props_.id}
          color="secondary"
          type="datetime-local"
          onChange={({ target: { value } }) => props_.onChange(value ? new Date(value).toJSON() : undefined)}
          value={toISOLocale(props_.value)}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => props_.onChange(new Date().toJSON())}
                onMouseDown={(event) => event.preventDefault()}
                edge="end"
              >
                <Today />
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    )

  return (
    <BuiForm
      {...props}
      validator={validator}
      widgets={{
        DateTimeWidget: CustomDateTimeWidget()
      }}
    >
      {/* this is to prevent Submit button */}
      <div />
    </BuiForm>
  )
}

export type FormDialogProps = DialogProps & {
  formProps: Partial<FormProps> & { schema: RJSFSchema }
  actions: JSX.Element
}

export const FormDialog: React.FC<FormDialogProps> = ({ formProps, actions, ...props }) => {
  return (
    <Dialog {...props}>
      <DialogContent>
        <MyBuiForm {...formProps} />
      </DialogContent>
      <DialogActions>{actions}</DialogActions>
    </Dialog>
  )
}
