import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'AccessesList'
export const classes = {
  root: `${PREFIX}-root`,
  validatedRoot: `${PREFIX}-validatedRoot`,
  pendingRoot: `${PREFIX}-pendingRoot`,
  deniedRoot: `${PREFIX}-deniedRoot`,
  toReviewRoot: `${PREFIX}-toReviewRoot`,
  card: `${PREFIX}-card`,
  unitHeader: `${PREFIX}-unitHeader`,
  validatedHeader: `${PREFIX}-validatedHeader`,
  pendingHeader: `${PREFIX}-pendingHeader`,
  deniedHeader: `${PREFIX}-deniedHeader`,
  toReviewHeader: `${PREFIX}-toReviewHeader`,
  listTitle: `${PREFIX}-listTitle`,
  subHeaderContent: `${PREFIX}-subHeaderContent`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.root}`]: {
    marginBottom: '5px',
    marginTop: '5px',
    minWidth: 300,
    width: '100%',
    borderRadius: 24,
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)',
    [theme.breakpoints.down('sm')]: {
      borderRadius: 12,
      marginBottom: '5px',
      marginTop: '5px',
      width: '100%'
    }
  },
  [`& .${classes.validatedRoot}`]: {
    // border: `4px solid ${theme.palette.primary.main}`
    borderLeft: `4px solid ${theme.palette.primary.main}`,
    borderRight: `4px solid ${theme.palette.primary.light}`,
    borderBottom: `4px solid ${theme.palette.primary.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.pendingRoot}`]: {
    // border: `4px solid ${theme.palette.error.light}`
    borderLeft: `4px solid ${theme.palette.warning.main}`,
    borderRight: `4px solid ${theme.palette.warning.light}`,
    borderBottom: `4px solid ${theme.palette.warning.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.deniedRoot}`]: {
    // border: `4px solid ${theme.palette.error.main}`
    borderLeft: `4px solid ${theme.palette.error.main}`,
    borderRight: `4px solid ${theme.palette.error.light}`,
    borderBottom: `4px solid ${theme.palette.error.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.toReviewRoot}`]: {
    // border: `4px solid ${theme.palette.secondary.main}`
    borderLeft: `4px solid ${theme.palette.secondary.main}`,
    borderRight: `4px solid ${theme.palette.secondary.light}`,
    borderBottom: `4px solid ${theme.palette.secondary.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.card}`]: {
    padding: 0
  },
  [`& .${classes.unitHeader}`]: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    [theme.breakpoints.down('sm')]: {
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8
    }
  },
  [`& .${classes.validatedHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.primary.main} 0%, ${theme.palette.primary.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.primary.main, 1)} !important`
  },
  [`& .${classes.pendingHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.warning.main} 0%, ${theme.palette.warning.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.error.light, 1)} !important`
  },
  [`& .${classes.deniedHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.error.main} 0%, ${theme.palette.error.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.error.main, 1)} !important`
  },
  [`& .${classes.toReviewHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.secondary.main} 0%, ${theme.palette.secondary.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.secondary.main, 1)} !important`
  },
  [`& .${classes.listTitle}`]: {
    backgroundColor: alpha(theme.palette.secondary.main, 0.2),
    color: theme.palette.primary.contrastText,
    borderRadius: 12,
    padding: 5,
    [theme.breakpoints.down('sm')]: {
      borderRadius: 6
    }
  },
  [`& .${classes.subHeaderContent}`]: {
    width: '100%',
    maxWidth: '500px',
    margin: 0,
    padding: 5
  }
}))
