import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'
import { Grid, List, ListSubheader, Paper, Skeleton, SvgIconProps, Typography } from '@mui/material'

import { classes, Root } from './styles'
import Access from '../Access/Access'
import { AppDispatch, useAppSelector } from 'state'
import { fetchUsers } from 'state/users'
import { fetchServices } from 'state/intensiveCareServices'
import { Access as AccessType } from 'types'

type AccessesListType = 'validated' | 'denied' | 'pending' | 'toReview'

type AccessesListProps = {
  accesses: AccessType[]
  title: string
  type: AccessesListType
  actions?: { callback: (i: string) => void; Icon: React.FC<SvgIconProps> }[]
}

const AccessesList: React.FC<AccessesListProps> = ({ accesses, title, type, actions }) => {
  const { users, services } = useAppSelector((state) => ({
    users: state.users,
    services: state.intensiveCareServices
  }))
  const dispatch = useDispatch<AppDispatch>()

  useEffect(() => {
    if (users.status !== 'loading' && users.ids.length === 0) dispatch(fetchUsers({}))
    if (services.status !== 'loading' && services.ids.length === 0) dispatch(fetchServices())
  }, [])

  const headerClassNames: { [Key in AccessesListType]: string } = {
    denied: classes.deniedHeader,
    pending: classes.pendingHeader,
    toReview: classes.toReviewHeader,
    validated: classes.validatedHeader
  }

  const rootClassNames: { [Key in AccessesListType]: string } = {
    denied: classes.deniedRoot,
    pending: classes.pendingRoot,
    toReview: classes.toReviewRoot,
    validated: classes.validatedRoot
  }

  if (!accesses.length) return <></>

  return (
    <Root>
      <Paper className={`${classes.root} ${rootClassNames[type]}`}>
        <List
          className={classes.card}
          subheader={
            <ListSubheader component="div" className={`${classes.unitHeader} ${headerClassNames[type]}`}>
              <Grid container justifyContent="flex-start" spacing={2} className={classes.subHeaderContent}>
                <Grid item>
                  <Typography align="center" variant="h6" className={classes.listTitle}>
                    {title}
                  </Typography>
                </Grid>
              </Grid>
            </ListSubheader>
          }
        >
          {users.status === 'loading' || services.status === 'loading' ? (
            <>
              <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
              <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
              <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
            </>
          ) : (
            _.cloneDeep(accesses).map((access: AccessType) => (
              <Access
                key={access.uuid}
                access={access}
                user={users.entities[access.user]}
                service={services.entities[access.intensiveCareService]}
                actions={actions}
              />
            ))
          )}
        </List>
      </Paper>
    </Root>
  )
}

export default AccessesList
