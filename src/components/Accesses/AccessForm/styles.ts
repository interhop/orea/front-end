import { styled } from '@mui/material/styles'

const PREFIX = 'AccessForm'
export const classes = {
  root: `${PREFIX}-root`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`&.${classes.root}`]: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
    padding: theme.spacing(2),
    // border: `4px solid ${theme.palette.secondary.main}`,
    borderRadius: '24px !important',
    minWidth: '80%',
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15) !important',
    backgroundColor: theme.palette.background.paper,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      minWidth: 0,
      borderTop: `4px solid ${theme.palette.secondary.main}`,
      borderBottom: `4px solid ${theme.palette.secondary.main}`,
      borderRadius: 12
    }
  }
}))
