import { styled } from '@mui/material/styles'

const PREFIX = 'TodoBed'
export const classes = {
  accordion: `${PREFIX}-accordion`,
  bedItem: `${PREFIX}-bedItem`,
  expansionSummary: `${PREFIX}-expansionSummary`,
  text: `${PREFIX}-text`,
  bedIndex: `${PREFIX}-bedIndex`,
  highSeverity: `${PREFIX}-highSeverity`,
  mediumSeverity: `${PREFIX}-mediumSeverity`,
  secondaryText: `${PREFIX}-secondaryText`,
  thirdDetails: `${PREFIX}-thirdDetails`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.accordion}`]: {
    margin: theme.spacing(2),
    borderRadius: 12,
    [theme.breakpoints.down('sm')]: {
      borderRadius: 6
    },
    ['& .Mui-expanded']: {
      margin: theme.spacing(2)
    }
  },
  [`& .${classes.expansionSummary}`]: {
    ['& .Mui-expanded']: {
      margin: theme.spacing(4, 2, 4, 2)
    }
  },
  [`& .${classes.bedItem}`]: {
    height: theme.spacing(30),
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      height: 'auto'
    }
  },
  [`& .${classes.bedIndex}`]: {
    margin: theme.spacing(5),
    height: '2.5rem',
    width: '2rem'
  },
  [`& .${classes.highSeverity}`]: {
    border: `2px solid ${theme.palette.error.dark}`,
    backgroundColor: theme.palette.error.main,
    color: theme.palette.secondary.contrastText,
    borderRadius: 12
  },
  [`& .${classes.mediumSeverity}`]: {
    border: `2px solid ${theme.palette.warning.dark}`,
    backgroundColor: theme.palette.warning.main,
    color: theme.palette.secondary.contrastText,
    borderRadius: 12
  },
  [`& .${classes.thirdDetails}`]: {
    padding: 2,
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  [`& .${classes.text}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.secondaryText}`]: {
    color: theme.palette.text.disabled
  }
}))
