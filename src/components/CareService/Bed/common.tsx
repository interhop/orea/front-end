import React, { MouseEventHandler } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import Grid from '@mui/material/Unstable_Grid2'
import IconButton from '@mui/material/IconButton'
import { SvgIconProps } from '@mui/material/SvgIcon'
import Tooltip from '@mui/material/Tooltip'
import Typography from '@mui/material/Typography'

import { classes, Root } from './styles'
import {
  BioChemicalFailureIcon,
  BrainFailureIcon,
  FemaleIcon,
  HeartFailureIcon,
  HematologicFailureIcon,
  KidneyFailureIcon,
  LiverFailureIcon,
  LungFailureIcon,
  MaleIcon,
  PositiveDetectionIcon
} from 'assets/icons'
import { dateToStr, displayName, getAge } from 'assets/utils'
import { AppDispatch } from 'state'
import { openDetectionAdd, openFailureDial } from 'state/modals'
import { Patient, PatientFailure, PatientDetection, FailureType, Stay } from 'types'

const failuresIcons: { [Key in FailureType]: React.FC<SvgIconProps> } = {
  heart: HeartFailureIcon,
  metabolic: BioChemicalFailureIcon,
  brain: BrainFailureIcon,
  lung: LungFailureIcon,
  kidney: KidneyFailureIcon,
  liver: LiverFailureIcon,
  hematologic: HematologicFailureIcon,
  detection: PositiveDetectionIcon
}

const failuresTitles: { [Key in FailureType]: string } = {
  heart: 'Défaillance cardiaque',
  metabolic: 'Défaillance métabolique',
  brain: 'Défaillance cérébrale',
  lung: 'Défailllance pulmonaire',
  kidney: 'Défaillance rénale',
  liver: 'Défaillance hépatique',
  hematologic: 'Défaillance hématologique',
  detection: 'Dernier dépistage positif'
}

export const getPatientAge = (p: Patient): string => {
  return p.birthDate ? `${getAge(p.birthDate)} ans` : ''
}

type GridIconProps = {
  Icon: React.FC<SvgIconProps>
  title: string
  failure?: Partial<PatientFailure>
  onClick?: MouseEventHandler
  displayInactive?: boolean
  IconProps?: Partial<SvgIconProps>
}

const GridIcon: React.FC<GridIconProps> = ({ failure, Icon, title, displayInactive, IconProps, ...props }) => {
  const actualTitle = `${title}${failure?.active ? ': ' + failure.indication : ''}`

  return (
    <Grid container justifyContent="center" xs={3} sm={3} {...props} style={{ padding: 2 }}>
      <Tooltip title={actualTitle} arrow>
        <span>
          <IconButton onClick={props.onClick} className={classes.failureIconButton}>
            {failure?.active ? (
              <Icon className={classes.failureIcon} {...IconProps} />
            ) : (
              <Icon
                className={`${classes.failureIcon} ${displayInactive ? '' : classes.transparentIcon} ${
                  classes.inactiveIcon
                }`}
                {...IconProps}
              />
            )}
          </IconButton>
        </span>
      </Tooltip>
    </Grid>
  )
}

type FailuresIconsGridsProps = {
  failures?: PatientFailure[]
  detections?: PatientDetection[]
  displayInactive?: boolean
  onlyActivated?: boolean
  stay?: Stay
  IconProps?: Partial<SvgIconProps>
  readOnly?: boolean
}

export const FailuresIconsGrids: React.FC<FailuresIconsGridsProps> = ({
  stay,
  IconProps,
  readOnly,
  failures = [],
  detections = [],
  displayInactive = false,
  onlyActivated = false
}) => {
  const dispatch = useDispatch<AppDispatch>()
  const activeFailures = failures.filter(_.matches({ active: true }))

  const keys = _.without(_.keys(failuresIcons), 'detection') as FailureType[]

  const detection = mostRecentDetection(detections)
  const hasDetection = detection && (detection.bhc || detection.blse || detection.sarm || detection.clostridium)

  return (
    <Root style={{ height: '100%', width: '100%' }}>
      <Grid container spacing={1} justifyContent="flex-start">
        {onlyActivated ? (
          <>
            {activeFailures.map((f) => (
              <GridIcon
                key={f.failureType}
                Icon={failuresIcons[f.failureType]}
                IconProps={IconProps}
                title={failuresTitles[f.failureType]}
                failure={f}
                onClick={!readOnly ? () => stay && dispatch(openFailureDial(f)) : undefined}
              />
            ))}
            {hasDetection && (
              <GridIcon
                key={'detection'}
                Icon={failuresIcons.detection}
                IconProps={IconProps}
                title={failuresTitles.detection}
                failure={{
                  active: hasDetection,
                  failureType: 'detection',
                  indication: ['bhc', 'blse', 'sarm', 'clostridium']
                    .filter((t) => _.propertyOf(detection)(t))
                    // .filter((t) => detection[t])
                    .join(', ')
                }}
                onClick={!readOnly ? () => stay && dispatch(openDetectionAdd(stay)) : undefined}
              />
            )}
          </>
        ) : (
          <>
            {keys.map((k) => (
              <GridIcon
                key={k}
                Icon={failuresIcons[k]}
                IconProps={IconProps}
                title={failuresTitles[k]}
                failure={_.find(activeFailures, (f) => f.failureType === k)}
                displayInactive={displayInactive}
                onClick={
                  !readOnly
                    ? () =>
                        stay &&
                        dispatch(
                          openFailureDial(
                            _.find(failures, (f) => f.failureType === k) ?? {
                              failureType: k,
                              stay: stay.uuid,
                              indication: '',
                              active: true,
                              measureDatetime: new Date().toISOString()
                            }
                          )
                        )
                    : undefined
                }
              />
            ))}
            <GridIcon
              key={'detection'}
              displayInactive={displayInactive}
              Icon={failuresIcons.detection}
              IconProps={IconProps}
              title={failuresTitles.detection}
              failure={
                detection && {
                  failureType: 'detection',
                  active: detection.bhc || detection.blse || detection.sarm || detection.clostridium,
                  indication: `${['bhc', 'blse', 'sarm', 'clostridium']
                    // .filter((t) => detection[t])
                    .filter((t) => _.propertyOf(detection)(t))
                    .join(', ')} - Le ${dateToStr(detection.measureDatetime)}`
                }
              }
              onClick={!readOnly ? () => stay && dispatch(openDetectionAdd(stay)) : undefined}
            />
          </>
        )}
      </Grid>{' '}
    </Root>
  )
}

export const PatientDetailPrimary = (patient: Patient): JSX.Element => (
  <>
    <Grid container alignItems="center">
      <Typography variant="body1" display="flex" alignItems="center">
        {patient.sex === 'w' ? (
          <FemaleIcon className={classes.info} />
        ) : patient.sex === 'm' ? (
          <MaleIcon className={classes.info} />
        ) : (
          ''
        )}
        {displayName(patient)}
      </Typography>
    </Grid>
    <Grid container alignItems="center" marginLeft="0.7rem">
      <Typography variant="body1">{getPatientAge(patient)}</Typography>
    </Grid>
  </>
)

export const mostRecentDetection = (detections: PatientDetection[]): PatientDetection | undefined => {
  return _.maxBy(detections, (val) => val.measureDatetime && new Date(val.measureDatetime).valueOf())
}

export const hasPostitiveDetection = (detections: PatientDetection[]): boolean => {
  const mostRecent = _.maxBy(detections, (val) => val.measureDatetime && new Date(val.measureDatetime).valueOf())

  return mostRecent?.bhc || mostRecent?.blse || mostRecent?.sarm || mostRecent?.clostridium || false
}
