import { styled } from '@mui/material/styles'

const PREFIX = 'Bed'
export const classes = {
  failureIcon: `${PREFIX}-failureIcon`,
  inactiveIcon: `${PREFIX}-inactiveIcon`,
  transparentIcon: `${PREFIX}-transparentIcon`,
  failureIconButton: `${PREFIX}-failureIconButton`,
  info: `${PREFIX}-info`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.failureIcon}`]: {
    width: '1em',
    height: '1em',
    color: theme.palette.error.main,
    [theme.breakpoints.down('sm')]: {
      width: '20px',
      maxWidth: '20px',
      height: '20px'
    }
  },
  [`& .${classes.inactiveIcon}`]: {
    color: theme.palette.text.disabled,
    '&:hover': {
      color: theme.palette.secondary.main
    }
  },
  [`& .${classes.transparentIcon}`]: {
    color: 'transparent'
  },
  [`& .${classes.failureIconButton}`]: {
    padding: '0px !important'
  },
  [`& .${classes.info}`]: {
    color: theme.palette.primary.main
  }
}))
