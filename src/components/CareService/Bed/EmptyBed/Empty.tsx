import React from 'react'
import { useDispatch } from 'react-redux'

import Grid from '@mui/material/Unstable_Grid2'
import ListItemButton from '@mui/material/ListItemButton'

import { classes, Root } from './styles'
import { AppDispatch } from 'state'
import { openBedUpdate } from 'state/modals'
import { Bed as BedType } from 'types'

type EmptyBedProps = {
  bed: BedType
}

const EmptyBed: React.FC<EmptyBedProps> = ({ bed }) => {
  const dispatch = useDispatch<AppDispatch>()

  const openDial = () => dispatch(openBedUpdate({ bed }))

  return (
    <Root>
      <ListItemButton
        key={bed.uuid}
        role={undefined}
        className={`${classes.bedItem} ${classes.text}`}
        onClick={openDial}
      >
        <Grid container alignItems="center" width="100%">
          <Grid container xs={1} className={classes.bedIndex} justifyContent="center" alignItems="center">
            {bed.unitIndex}
          </Grid>
          <Grid container xs={8} sm={4}>
            Ajouter un patient
          </Grid>
        </Grid>
      </ListItemButton>
    </Root>
  )
}

export default EmptyBed
