import { styled } from '@mui/material/styles'

const PREFIX = 'EmptyBed'
export const classes = {
  bedItem: `${PREFIX}-bedItem`,
  bedIndex: `${PREFIX}-bedIndex`,
  itemContent: `${PREFIX}-itemContent`,
  text: `${PREFIX}-text`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.bedItem}`]: {
    height: 'auto',
    width: '100%'
  },
  [`& .${classes.bedIndex}`]: {
    margin: theme.spacing(5),
    height: '2.5rem',
    width: '2rem'
  },
  [`& .${classes.itemContent}`]: {
    width: '50%',
    maxWidth: 'min(50%, 600px)'
  },
  [`& .${classes.text}`]: {
    color: theme.palette.text.primary
  }
}))
