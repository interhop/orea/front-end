import React, { TouchEventHandler, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import _ from 'lodash'

import Badge from '@mui/material/Badge'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Unstable_Grid2'
import IconButton from '@mui/material/IconButton'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction'
import Skeleton from '@mui/material/Skeleton'
import Tooltip from '@mui/material/Tooltip'
import Typography from '@mui/material/Typography'
import { Theme, useMediaQuery, useTheme } from '@mui/material'

import { classes, Root } from './styles'
import { FailuresIconsGrids, PatientDetailPrimary } from '../common'
import EmptyBed from '../EmptyBed/Empty'
import TodoIconButton from 'components/BedStay/TodoList/TodoIconButton'
import { AtcdIcon, MovePatientIcon } from 'assets/icons'
import { dateToDayStep } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { openBedSwap } from 'state/modals'
import { fetchStays } from 'state/stays'
import { Bed as BedType, Patient, State } from 'types'

const countPatientAtcd = (patient: Patient) =>
  _.keys(patient.allergies || {}).length + _.keys(patient.antecedents || {}).length

const buildAtcdTooltip = (patient: Patient): JSX.Element => {
  if (_.isEmpty(patient.antecedents) && _.isEmpty(patient.allergies))
    return <Typography variant="body2">Aucun antécédent ni allergie déclaré.e</Typography>

  return (
    <span style={{ verticalAlign: 'middle' }}>
      {!_.isEmpty(patient.antecedents) && (
        <>
          <Typography variant="body1">Antécédents</Typography>
          {_.entries(patient.antecedents).map(([k, v]) => (
            <Typography key={`atcd-${k}`} variant="body2">
              {k}
              {v ? `: ${v}` : ''}
            </Typography>
          ))}
        </>
      )}
      {!_.isEmpty(patient.allergies) && (
        <>
          <Typography variant="body1">Allergies</Typography>
          {_.entries(patient.allergies).map(([k, v]) => (
            <Typography key={`allergie-${k}`} variant="body2">
              {k}
              {v ? `: ${v}` : ''}
            </Typography>
          ))}
        </>
      )}
    </span>
  )
}

type DefaultBedProps = {
  bed: State<BedType>
}

const DefaultBed: React.FC<DefaultBedProps> = ({ bed }) => {
  const th: Theme = useTheme()
  const smallScreen = useMediaQuery(th.breakpoints.down('sm'))
  const touchableScreen = 'ontouchstart' in window || navigator.maxTouchPoints > 0

  const navigate = useNavigate()
  const dispatch = useDispatch<AppDispatch>()
  const { stays } = useAppSelector((state) => state)

  const [displaySecActions, setDisplaySecActions] = React.useState(false)

  let preventNextClick = false

  useEffect(() => {
    if (bed.currentPatientStay && !_.includes(stays.ids, bed.currentPatientStay) && stays.status !== 'loading') {
      dispatch<any>(fetchStays({ uuid: bed.currentPatientStay }))
    }
  }, [])

  const handlePatientClick = (e?: React.MouseEvent<HTMLElement>) => {
    if (!preventNextClick) {
      const url = `/beds/${bed.uuid}`
      if (e?.ctrlKey) {
        window.open(url, '_blank')
      } else {
        navigate(url)
      }
    }
    preventNextClick = false
  }

  let holdTimeout: ReturnType<typeof setTimeout> | string | number
  const startHold: TouchEventHandler = () => {
    if (!touchableScreen) return
    holdTimeout && clearTimeout(holdTimeout)
    holdTimeout = setTimeout(() => {
      // _displaySecActions = !_displaySecActions;
      setDisplaySecActions(!displaySecActions)
    }, 1000)
  }

  const endHold: TouchEventHandler = (e) => {
    if (!touchableScreen) return
    if (holdTimeout) {
      clearTimeout(holdTimeout)
    } else {
      preventNextClick = true
      e.preventDefault()
    }
  }

  if (bed.loading) return <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
  if (!bed.currentPatientStay) return <EmptyBed bed={bed} />
  const stay = stays.entities[bed.currentPatientStay]
  const patient: Patient = stay?.patient as Patient
  if (!stay && stays.status === 'loading') return <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
  if (!stay || !stay.patient) return <EmptyBed bed={bed} />

  return (
    <Root>
      <ListItemButton
        key={bed.uuid}
        className={`${classes.bedItem} ${classes.text}`}
        onTouchStartCapture={startHold}
        onTouchEndCapture={endHold}
      >
        <Grid container alignItems="center" width="100%">
          <Grid
            container
            xs={1}
            className={`${classes.bedIndex} ${
              stay.severity === 0 ? classes.highSeverity : stay.severity === 1 ? classes.mediumSeverity : ''
            }`}
            justifyContent="center"
            alignItems="center"
          >
            {bed.unitIndex}
          </Grid>
          <Grid container xs={8} sm={4}>
            <Grid container xs={12} spacing={1} alignContent="center" onClick={handlePatientClick}>
              {PatientDetailPrimary(patient)}
            </Grid>
            <Grid container xs={12} className={classes.secondaryText} onClick={handlePatientClick}>
              <Typography
                variant={stay.hospitalisationCause && stay.hospitalisationCause.length > 30 ? 'body2' : 'body1'}
              >
                {dateToDayStep(stay.startDate)} - {stay.hospitalisationCause}
              </Typography>
            </Grid>
            {smallScreen && (
              <Grid container xs={12}>
                <Box className={classes.thirdDetails}>
                  <FailuresIconsGrids
                    displayInactive
                    failures={stay.failureMeasures}
                    detections={stay.detections}
                    stay={stay}
                  />
                </Box>
              </Grid>
            )}
          </Grid>
          {!smallScreen && (
            <Grid container xs={4} className={classes.thirdDetails}>
              <FailuresIconsGrids failures={stay.failureMeasures} detections={stay.detections} stay={stay} />
            </Grid>
          )}
          <Grid
            container
            xs={1}
            sm="auto"
            className={classes.additionalDetails}
            justifyContent="center"
            alignItems="center"
          >
            <Tooltip title={buildAtcdTooltip(patient)} arrow>
              <Badge
                showZero={false}
                color="primary"
                badgeContent={countPatientAtcd(patient)}
                slotProps={{
                  badge: {
                    style: {
                      top: 'unset',
                      right: 'unset',
                      transform: 'scale(1) translate(40%, 135%)'
                    }
                  }
                }}
              >
                <AtcdIcon
                  className={`${classes.rowIcon} ${countPatientAtcd(patient) ? classes.info : classes.disabled}`}
                />
              </Badge>
            </Tooltip>
            <TodoIconButton
              stayId={stay.uuid}
              buttonProps={{ className: classes.todoButton }}
              className={`${classes.rowIcon} ${classes.action}`}
            />{' '}
          </Grid>
          {/*<Grid container xs={1} className={classes.additionalDetails}>*/}
          {/*  <TodoIconButton stayId={stay.uuid} className={`${classes.rowIcon} ${classes.action}`} />{' '}*/}
          {/*</Grid>*/}
        </Grid>
        <ListItemSecondaryAction>
          <IconButton onClick={() => dispatch(openBedSwap(stay))} color="secondary">
            <MovePatientIcon className={classes.actionIcon} />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemButton>
    </Root>
  )
}

export default DefaultBed
