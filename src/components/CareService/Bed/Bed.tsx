import React, { useEffect } from 'react'

import { Skeleton, Typography } from '@mui/material'

import DefaultBed from './DefaultBed/Default'
import TodoBed from './TodoBed/Todo'
import PrintSelectionBed from './SelectedBed/PrintSelection'
import { useAppDispatch, useAppSelector } from 'state'
import { fetchBeds } from 'state/beds'

const Bed: React.FC<{ bedId: string }> = ({ bedId }) => {
  const dispatch = useAppDispatch()
  const { beds, display } = useAppSelector((state) => ({
    beds: state.beds,
    display: state.settings
  }))

  const bed = beds.entities[bedId]

  useEffect(() => {
    if (!(bedId in beds.entities) && beds.status !== 'loading') {
      dispatch<any>(
        fetchBeds({
          filters: {
            uuid: bedId
          }
        })
      )
    }
  }, [])

  if (beds.status === 'loading') {
    return <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
  }

  if (!bed) {
    return <Typography>Lit introuvable</Typography>
  }

  return display.printMode ? (
    <PrintSelectionBed bed={bed} />
  ) : display.todoMode ? (
    <TodoBed bed={bed} />
  ) : (
    <DefaultBed bed={bed} />
  )
}

export default Bed
