import React from 'react'

import { Grid, MenuItem, Select, Typography } from '@mui/material'

import { classes, Root } from './styles'
import { IntensiveCareService } from 'types'

type CareServiceTitleProps = {
  service: IntensiveCareService
  otherServices: IntensiveCareService[]
  changeService?: (id: string) => void
}

const CareServiceTitle: React.FC<CareServiceTitleProps> = ({ service, otherServices, changeService }) => {
  return (
    <Root>
      <Grid item container className={classes.card} justifyContent="center" direction="column">
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={service.uuid}
          onChange={(d) => changeService && changeService(d.target.value)}
        >
          {otherServices.map((s) => (
            <MenuItem key={s.uuid} value={s.uuid}>
              <Typography align="center" variant="h2" className={classes.titleText}>
                Hôpital {s.hospitalName}
              </Typography>
              <Typography align="center" variant="h3" className={classes.titleText}>
                Service {s.name}
              </Typography>
            </MenuItem>
          ))}
          <MenuItem key="new" onClick={() => window.open(`/accesses`, '_blank')}>
            Gérer les accès
          </MenuItem>
        </Select>
      </Grid>
    </Root>
  )
}
export default CareServiceTitle
