import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'Unit'
export const classes = {
  root: `${PREFIX}-root`,
  unitHeader: `${PREFIX}-unitHeader`,
  unitTitle: `${PREFIX}-unitTitle`,
  subHeaderContent: `${PREFIX}-subHeaderContent`,
  icon: `${PREFIX}-icon`,
  actionIcon: `${PREFIX}-actionIcon`,
  infoIcon: `${PREFIX}-infoIcon`,
  info: `${PREFIX}-info`,
  infoValue: `${PREFIX}-infoValue`,
  list: `${PREFIX}-list`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.root}`]: {
    width: '100%',
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 24,
    borderLeft: `4px solid ${theme.palette.secondary.main}`,
    borderRight: `4px solid ${theme.palette.secondary.light}`,
    borderBottom: `4px solid ${theme.palette.secondary.light}`,
    [theme.breakpoints.down('sm')]: {
      minWidth: 0,
      borderLeft: 0,
      borderRight: 0,
      borderRadius: 12
    }
  },
  [`& .${classes.unitHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.secondary.main} 0%, ${theme.palette.secondary.light} 100%)`,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    [theme.breakpoints.down('sm')]: {
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8
    }
  },
  [`& .${classes.unitTitle}`]: {
    color: `${theme.palette.secondary.contrastText} !important`,
    padding: theme.spacing(2, 4, 2, 4)
  },
  [`& .${classes.subHeaderContent}`]: {
    width: '100%',
    maxWidth: '500px',
    margin: 0,
    padding: 5
  },
  [`& .${classes.info}`]: {
    backgroundColor: alpha(theme.palette.secondary.dark, 0.2),
    color: `${theme.palette.secondary.contrastText} !important`,
    padding: theme.spacing(2, 4, 2, 4)
  },
  [`& .${classes.infoIcon}`]: {
    color: `${theme.palette.secondary.contrastText} !important`
  }
}))
