import _ from 'lodash'
import { Dictionary } from '@reduxjs/toolkit'
import { CustomValidator, RJSFSchema, UiSchema } from '@rjsf/utils'

import { displayName } from 'assets/utils'
import { PrintSettings } from 'state/settings'
import { Access, Bed, FrontInput, IntensiveCareService, Patient, Stay, TreatmentLimitation, Unit } from 'types'

type BedDialFormProps = {
  localId: string
  stayStartDate: string
  hospitalisationCause: string
  familyName: string
  firstName: string
  birthDate: string
  sex: Patient['sex']
  severity: Stay['severity']
  weightKg: number | null
  sizeCm: number | null
}

export const patientAddFormDefault: BedDialFormProps = {
  localId: '',
  stayStartDate: new Date().toISOString().split('T')[0],
  hospitalisationCause: '',
  familyName: '',
  firstName: '',
  birthDate: '',
  sex: 'u',
  severity: 2,
  weightKg: null,
  sizeCm: null
}

export const patientAddFormSchemaShort: RJSFSchema = {
  title: 'Ajout de patient',
  description: 'Informations de base pour le patient',
  type: 'object',
  required: ['localId'],
  properties: {
    localId: {
      type: 'string',
      title: 'Identifiant NIP (10 chiffres)',
      maxLength: 10,
      pattern: '\\[0-9]*'
    },
    stayStartDate: {
      type: 'string',
      format: 'date',
      title: 'Premier jour de réanimation'
    }
  }
}

export const patientAddFormSchema: RJSFSchema = {
  ...patientAddFormSchemaShort,
  properties: {
    ...patientAddFormSchemaShort.properties,
    hospitalisationCause: {
      type: 'string',
      title: "Motif d'hospitalisation"
    },
    lastName: {
      type: 'string',
      title: 'Nom'
    },
    firstName: {
      type: 'string',
      title: 'Prénom'
    },
    birthDate: {
      type: 'string',
      format: 'date',
      title: 'Date de naissance'
    },
    sex: {
      title: 'Sexe',
      type: 'string',
      oneOf: [
        { enum: ['w'], title: 'Femme' },
        { enum: ['m'], title: 'Homme' },
        { enum: ['u'], title: 'Non défini' }
      ]
    },
    severity: {
      title: 'Gravité (',
      type: 'number',
      default: 2,
      oneOf: [
        { enum: [0], title: 'A risques' },
        { enum: [1], title: 'Instable' },
        { enum: [2], title: 'Stable' }
      ]
    },
    weightKg: {
      title: 'Poids',
      type: 'string',
      minimum: 0
    },
    sizeCm: {
      title: 'Taille (cm)',
      type: 'string',
      minimum: 0
    }
  }
}

export const patientAddFormGetErrSchema = (
  data?: Partial<Patient> & { stayStartDate: string }
): Dictionary<string | false> => {
  if (!data) return {}

  return {
    localId: (!data.localId || !/^[0-9]*$/.test(data.localId)) && 'NIP doit être numérique',
    stayStartDate:
      data.stayStartDate &&
      new Date(data.stayStartDate).valueOf() > new Date().valueOf() &&
      'Le séjour ne peut pas commencer dans le futur',
    lastName:
      data.lastName &&
      !/^[A-zÀ-ÖØ-öø-ÿ-' ]*$/.test(data.lastName) &&
      'Caractère non pris en charge dans le nom de famille',
    firstName:
      data.firstName && !/^[A-zÀ-ÖØ-öø-ÿ-' ]*$/.test(data.firstName) && 'Caractère non pris en charge dans le prénom',
    birthDate:
      data.birthDate &&
      new Date(data.birthDate).valueOf() > new Date().valueOf() &&
      'La date de naissance doit être passée',
    weightKg:
      data.weightKg !== null &&
      data.weightKg !== undefined &&
      !/^[0-9]{0,3}(\.[0-9]{0,3})?$/.test(data.weightKg.toString()) &&
      'Le poids doit être au format xxx.xxx',
    sizeCm:
      data.sizeCm !== null &&
      data.sizeCm !== undefined &&
      !/^[0-9]{0,3}(\.[0-9]{0,2})?$/.test(data.sizeCm.toString()) &&
      'La taille doit être au format xxx.xx'
  }
}

export const patientAddFormValidate =
  (full: boolean): CustomValidator<Partial<Patient> & { stayStartDate: string }> =>
  (form, errors) => {
    const errSchema = patientAddFormGetErrSchema(form)
    _.entries(errSchema)
      .filter(([k]) => full || k in ['localId', 'stayStartDate'])
      .forEach(([, v]) => {
        if (v) {
          const errs = _.propertyOf(errors)(v) as { addError: (message: string) => void }
          if (errs) errs.addError(v)
        }
      })

    return errors
  }

export const uiPatientAddFormSchema: UiSchema = {
  uuid: { 'ui:widget': 'hidden' },
  localId: { 'ui:autofocus': true }
}

export const schemaSwapFormSchema = (
  patient: Patient,
  units: Unit[],
  beds: Dictionary<Bed>,
  stays: Dictionary<Stay>,
  currentStay: Stay
): RJSFSchema => ({
  title: `Déplacer ${patient.firstName} ${patient.lastName}`,
  description: 'Choisissez un nouveau lit',
  type: 'object',
  required: ['bedId'],
  properties: {
    bedId: {
      oneOf: units.map((u) => ({
        title: u.name,
        type: 'string',
        oneOf: _.without(u.beds || [], currentStay.bed)
          .filter(_.propertyOf(beds))
          .map((bedId) => ({
            enum: [bedId || ''],
            title: `${beds[bedId]?.unitIndex} - ${
              beds[bedId || '']?.currentPatientStay
                ? displayName(stays[beds[bedId || '']?.currentPatientStay || '']?.patient as Patient)
                : 'Libre'
            }`
          }))
      }))
    }
  }
})

type BedSwapFormProps = {
  bedId: string
}

export const schemaSwapFormDefault: BedSwapFormProps = {
  bedId: ''
}

export const uiSchemaSwapForm: UiSchema = {
  bedId: {
    // 'ui:hidden': true
  }
}

export type FailureUpdateFormProps = {
  active: boolean
  indication: string
  measureDatetime: string
}

export const schemaFailureUpdateFormDefault: FailureUpdateFormProps = {
  active: false,
  indication: '',
  measureDatetime: ''
}

export const schemaFailureUpdateSchema = (failureName: string, patientName: string): RJSFSchema => ({
  title: `Défaillance ${failureName}`,
  description: `Ajouter la défaillance ${failureName} au patient ${patientName}`,
  type: 'object',
  // required: [],
  properties: {
    active: {
      type: 'boolean',
      title: 'Active'
    },
    indication: {
      type: 'string',
      title: 'Indication'
    },
    measureDatetime: {
      type: 'string',
      format: 'datetime',
      title: 'Date'
    }
  }
})

export const uiSchemaFailureUpdateForm: UiSchema = {
  measureDatetime: {
    ['ui:widget']: 'datetime'
  }
  // measureDatetime: {
  //   'ui:field': 'date'
  // }
}

type DetectionFormProps = {
  date: string
  blse: boolean
  sarm: boolean
  bhc: boolean
  clostridium: boolean
}

export const detectionAddFormDefault = (): DetectionFormProps => ({
  date: new Date().toISOString(),
  blse: false,
  sarm: false,
  bhc: false,
  clostridium: false
})

export const detectionAddFormSchema: RJSFSchema = {
  title: `Nouveau dépistage`,
  description: "Ajouter les résultats d'un nouveau dépistage",
  type: 'object',
  required: ['date'],
  properties: {
    date: {
      title: 'Date du dépistage',
      type: 'string',
      format: 'datetime'
    },
    blse: {
      title: 'BLSE',
      type: 'boolean'
    },
    sarm: {
      title: 'SARM',
      type: 'boolean'
    },
    bhc: {
      title: 'BHC',
      type: 'boolean'
    },
    clostridium: {
      title: 'Clostridium',
      type: 'boolean'
    }
  }
}

export const uiDetectionAddFormSchema: UiSchema = {}

export type TreatmentLimitationFormProps = {
  therapeutics?: {
    noAcrReanimation?: boolean
    noNewFailuresOrTherapRaiseTreatment?: boolean
    noCatecholamines?: boolean
    noIntubation?: boolean
    noAssistedVentilation?: boolean
    noO2?: boolean
    noEer?: boolean
    noTransfusion?: boolean
    noSurgery?: boolean
    noPICOrDVE?: boolean
    noNewAntibiotherapy?: boolean
    noSirurgicalReintervetion?: boolean
    noNewComplementaryExams?: boolean
    noBiologicalResults?: boolean
  }
  therapeuticsStops?: {
    pressorAminesStop?: boolean
    eerStop?: boolean
    fio221percent?: boolean
    oxygenotherapyStop?: boolean
    mecanicVentilationStop?: boolean
    extubation?: boolean
    nutritionStop?: boolean
    hydratationStop?: boolean
    antibioticStop?: boolean
    dveAblation?: boolean
    ecmoStop?: boolean
    allCurrentTherapeuticsStop?: boolean
    otherStops?: string
  }
  currentTherapeutics?: {
    fio2Limit?: number
    noMecanicVentilationMarkup?: boolean
    aminesLimitation?: number
  }
  icuAdmission?: {
    noReanimationAdmittance?: boolean
  }
  other?: {
    treatmentLimitationsComments?: string
  }
}

export const treatmentLimitationsFormDataToObject = (
  d: TreatmentLimitationFormProps
): Partial<TreatmentLimitation> => ({
  noAcrReanimation: d.therapeutics?.noAcrReanimation,
  noNewFailuresOrTherapRaiseTreatment: d.therapeutics?.noNewFailuresOrTherapRaiseTreatment,
  noCatecholamines: d.therapeutics?.noCatecholamines,
  noIntubation: d.therapeutics?.noIntubation,
  noAssistedVentilation: d.therapeutics?.noAssistedVentilation,
  noO2: d.therapeutics?.noO2,
  noEer: d.therapeutics?.noEer,
  noTransfusion: d.therapeutics?.noTransfusion,
  noSurgery: d.therapeutics?.noSurgery,
  noPICOrDVE: d.therapeutics?.noPICOrDVE,
  noNewAntibiotherapy: d.therapeutics?.noNewAntibiotherapy,
  noSirurgicalReintervetion: d.therapeutics?.noSirurgicalReintervetion,
  noNewComplementaryExams: d.therapeutics?.noNewComplementaryExams,
  noBiologicalResults: d.therapeutics?.noBiologicalResults,
  pressorAminesStop: d.therapeuticsStops?.pressorAminesStop,
  eerStop: d.therapeuticsStops?.eerStop,
  fio221percent: d.therapeuticsStops?.fio221percent,
  oxygenotherapyStop: d.therapeuticsStops?.oxygenotherapyStop,
  mecanicVentilationStop: d.therapeuticsStops?.mecanicVentilationStop,
  extubation: d.therapeuticsStops?.extubation,
  nutritionStop: d.therapeuticsStops?.nutritionStop,
  hydratationStop: d.therapeuticsStops?.hydratationStop,
  antibioticStop: d.therapeuticsStops?.antibioticStop,
  dveAblation: d.therapeuticsStops?.dveAblation,
  ecmoStop: d.therapeuticsStops?.ecmoStop,
  allCurrentTherapeuticsStop: d.therapeuticsStops?.allCurrentTherapeuticsStop,
  otherStops: d.therapeuticsStops?.otherStops,
  fio2Limit: d.currentTherapeutics?.fio2Limit,
  noMecanicVentilationMarkup: d.currentTherapeutics?.noMecanicVentilationMarkup,
  aminesLimitation: d.currentTherapeutics?.aminesLimitation,
  noReanimationAdmittance: d.icuAdmission?.noReanimationAdmittance,
  treatmentLimitationsComments: d.other?.treatmentLimitationsComments
})

export const treatmentLimitationsObjectToFormData = (
  d?: FrontInput<TreatmentLimitation>
): Partial<TreatmentLimitationFormProps> | undefined =>
  !d
    ? undefined
    : {
        therapeutics: {
          noAcrReanimation: d.noAcrReanimation,
          noNewFailuresOrTherapRaiseTreatment: d.noNewFailuresOrTherapRaiseTreatment,
          noCatecholamines: d.noCatecholamines,
          noIntubation: d.noIntubation,
          noAssistedVentilation: d.noAssistedVentilation,
          noO2: d.noO2,
          noEer: d.noEer,
          noTransfusion: d.noTransfusion,
          noSurgery: d.noSurgery,
          noPICOrDVE: d.noPICOrDVE,
          noNewAntibiotherapy: d.noNewAntibiotherapy,
          noSirurgicalReintervetion: d.noSirurgicalReintervetion,
          noNewComplementaryExams: d.noNewComplementaryExams,
          noBiologicalResults: d.noBiologicalResults
        },
        therapeuticsStops: {
          pressorAminesStop: d.pressorAminesStop,
          eerStop: d.eerStop,
          fio221percent: d.fio221percent,
          oxygenotherapyStop: d.oxygenotherapyStop,
          mecanicVentilationStop: d.mecanicVentilationStop,
          extubation: d.extubation,
          nutritionStop: d.nutritionStop,
          hydratationStop: d.hydratationStop,
          antibioticStop: d.antibioticStop,
          dveAblation: d.dveAblation,
          ecmoStop: d.ecmoStop,
          allCurrentTherapeuticsStop: d.allCurrentTherapeuticsStop,
          otherStops: d.otherStops
        },
        currentTherapeutics: {
          fio2Limit: d.fio2Limit,
          noMecanicVentilationMarkup: d.noMecanicVentilationMarkup,
          aminesLimitation: d.aminesLimitation
        },
        icuAdmission: { noReanimationAdmittance: d.noReanimationAdmittance },
        other: { treatmentLimitationsComments: d.treatmentLimitationsComments }
      }

//
// export const treatmentLimitationFormDefault = (): TreatmentLimitationFormProps => ({})

export const treatmentLimitationFormSchema: RJSFSchema = {
  title: `Limitation de traitements`,
  // description: "Ajouter les résultats d'un nouveau dépistage",
  type: 'object',
  properties: {
    therapeutics: {
      title: `Limitation des thérapeutiques`,
      type: 'object',
      properties: {
        noAcrReanimation: {
          title: "Pas de réanimation de l'ACR",
          type: 'boolean',
          default: false
        },
        noNewFailuresOrTherapRaiseTreatment: {
          title: 'Pas de traitement de nouvelles défaillances ou escalade thérapeutique',
          type: 'boolean',
          default: false
        },
        noCatecholamines: {
          title: 'Pas de catécholamines',
          type: 'boolean',
          default: false
        },
        noIntubation: {
          title: "Pas d'intubation",
          type: 'boolean',
          default: false
        },
        noAssistedVentilation: {
          title: 'Pas de VNI',
          type: 'boolean',
          default: false
        },
        noO2: {
          title: 'Pas d’O2',
          type: 'boolean',
          default: false
        },
        noEer: {
          title: "Pas d'EER",
          type: 'boolean',
          default: false
        },
        noTransfusion: {
          title: 'Pas de transfusion ',
          type: 'boolean',
          default: false
        },
        noSurgery: {
          title: 'Pas de chirurgie',
          type: 'boolean',
          default: false
        },
        noPICOrDVE: {
          title: 'Pas de PIC ou de DVE',
          type: 'boolean',
          default: false
        },
        noNewAntibiotherapy: {
          title: 'Pas de nouvelle antibiothérapie',
          type: 'boolean',
          default: false
        },
        noSirurgicalReintervetion: {
          title: 'Pas de ré-intervention chirurgicale',
          type: 'boolean',
          default: false
        },
        noNewComplementaryExams: {
          title: 'Pas de nouveaux examens complémentaires',
          type: 'boolean',
          default: false
        },
        noBiologicalResults: {
          title: 'Pas de bilan biologique',
          type: 'boolean',
          default: false
        }
      }
    },
    therapeuticsStops: {
      title: `Arrêt des thérapeutiques`,
      type: 'object',
      properties: {
        pressorAminesStop: {
          title: 'Arrêt des amines vasopressives',
          type: 'boolean',
          default: false
        },
        eerStop: {
          title: 'Arrêt de l’épuration extra-rénale',
          type: 'boolean',
          default: false
        },
        fio221percent: {
          title: 'Mise en FIO2  = 21%',
          type: 'boolean',
          default: false
        },
        oxygenotherapyStop: {
          title: 'Arrêt de l’oxygénothérapie',
          type: 'boolean',
          default: false
        },
        mecanicVentilationStop: {
          title: 'Arrêt de la ventilation mécanique',
          type: 'boolean',
          default: false
        },
        extubation: {
          title: 'Extubation ',
          type: 'boolean',
          default: false
        },
        nutritionStop: {
          title: 'Arrêt de la nutrition ',
          type: 'boolean',
          default: false
        },
        hydratationStop: {
          title: 'Arrêt de l’hydratation',
          type: 'boolean',
          default: false
        },
        antibioticStop: {
          title: 'Arrêt des antibiotiques',
          type: 'boolean',
          default: false
        },
        dveAblation: {
          title: 'Ablation de la DVE',
          type: 'boolean',
          default: false
        },
        ecmoStop: {
          title: 'Arrêt de l’ECMO',
          type: 'boolean',
          default: false
        },
        allCurrentTherapeuticsStop: {
          title: 'Arrêt de toutes les thérapeutiques en cours',
          type: 'boolean',
          default: false
        },
        otherStops: {
          title: 'Autres',
          type: 'string'
        }
      }
    },
    currentTherapeutics: {
      title: `Limitation des thérapeutiques en cours`,
      type: 'object',
      properties: {
        fio2Limit: {
          title: 'FiO2 limitée à (%)',
          type: 'number',
          minimum: 0,
          maximum: 100
        },
        noMecanicVentilationMarkup: {
          title: 'Pas de majoration de la VM (NO, DV,…)',
          type: 'boolean'
        },
        aminesLimitation: {
          title: 'Amines limitées à (ml/h)',
          type: 'number',
          minimum: 0
        }
      }
    },
    icuAdmission: {
      title: `Admission en réanimation`,
      type: 'object',
      properties: {
        noReanimationAdmittance: {
          title: 'Pas de (ré) admission en réanimation',
          type: 'boolean',
          default: false
        }
      }
    },
    other: {
      title: `Autres`,
      type: 'object',
      properties: {
        treatmentLimitationsComments: {
          title: 'Commentaires / évolution',
          type: 'string'
        }
      }
    }
  }
}

export const uiTreatmentLimitationFormSchema: UiSchema = {}

export const printSettingsFormDefault: PrintSettings = {
  components: [],
  onePatientPerPage: false,
  newPageForUnit: false
}

export const printSettingsFormSchema: RJSFSchema = {
  title: 'Préférences de création de PDF',
  description: 'Choisissez les éléments à imprimer',
  type: 'object',
  properties: {
    newPageForUnit: {
      title: 'Nouvelle feuille par unité',
      type: 'boolean',
      default: false
    },
    onePatientPerPage: {
      title: 'Imprimer uniquement un patient par page',
      type: 'boolean',
      default: false
    },
    components: {
      type: 'array',
      title: 'Infos à imprimer',
      uniqueItems: true,
      items: {
        type: 'string',
        oneOf: [
          { enum: ['detections'], title: 'Dépistages' },
          { enum: ['antecedents'], title: 'Antécédents' },
          { enum: ['allergies'], title: 'Allergies' },
          { enum: ['lat'], title: 'Limitations de traitements' },
          { enum: ['failures'], title: 'Défaillances' },
          { enum: ['todoList'], title: 'Todo List' },
          { enum: ['recentDiseaseHistory'], title: 'Histoire de la maladie récente' },
          { enum: ['evolution'], title: 'Evolution' },
          { enum: ['dailyNotes'], title: 'Notes du jour' },
          // { enum: ['measuresTable'], title: 'Tableau de mesures' },
          { enum: ['writingSpace'], title: 'Espace pour prise de notes' }
        ]
      },
      default: ['detections', 'antecedents', 'allergies', 'lat', 'failures', 'todoList']
    }
  }
}

export const uiPrintSettingsFormSchema = {
  components: {
    'ui:widget': 'checkboxes'
  }
  // nbDays: {
  //   'ui:widget': 'range'
  // }
}

export type NewAccessFormProps = {
  intensiveCareService?: string
  startDatetime?: string
  endDatetime?: string
  rightReview?: boolean
}

export const newAccessFormDefault: Partial<Access> = {}

export const newAccessFormSchema = (services: IntensiveCareService[]): RJSFSchema => ({
  title: 'Demander un nouvel accès',
  type: 'object',
  // required: ['intensiveCareService'],
  properties: {
    intensiveCareService: {
      title: 'Service',
      type: 'string',
      oneOf: services.map((s) => ({
        enum: [s.uuid],
        title: `${s.hospitalName} - ${s.name}`
      }))
    },
    startDatetime: {
      title: 'Date de début',
      type: 'string',
      format: 'date',
      default: _.now().toString()
    },
    endDatetime: {
      title: 'Date de fin',
      type: 'string',
      format: 'date'
    },
    rightReview: {
      title: "En tant qu'administrateur",
      type: 'boolean'
    }
  }
})

export const uiNewAccessFormSchema = {}
