import React from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import AddIcon from '@mui/icons-material/AddCircle'

import { Root } from './styles'
import MyDialogActions from './MyDialogActions/MyDialogActions'
import { FormDialog } from 'components/Form'
import {
  patientAddFormDefault,
  patientAddFormGetErrSchema,
  patientAddFormSchema,
  patientAddFormSchemaShort,
  patientAddFormValidate,
  uiPatientAddFormSchema
} from 'components/formSchemas'
import { closeBedUpdateDial } from 'state/modals'
import { AppDispatch, useAppSelector } from 'state'
import { createPatientAndStay } from 'state/stays'

const BedDialog: React.FC<any> = () => {
  const { modal } = useAppSelector((state) => ({
    modal: state.modals.bedUpdate
  }))
  const dispatch = useDispatch<AppDispatch>()

  const [formData, setFormData] = React.useState(patientAddFormDefault)

  const closeDial = () => {
    dispatch(closeBedUpdateDial())
  }

  const cancelDial = () => {
    setFormData(patientAddFormDefault)
    closeDial()
  }

  const submit = () =>
    _.values(patientAddFormGetErrSchema(formData)).some((b) => !!b)
      ? undefined
      : dispatch(
          createPatientAndStay({
            patient: modal.fullDisplay
              ? _.pick(
                  formData,
                  _.keys(formData).filter((k) => _.propertyOf(formData)(k))
                )
              : _.pick(formData, ['localId', 'stayStartDate']),
            bedId: modal.bed?.uuid || ''
          })
        )

  if (!modal.bed) return <></>

  return (
    <Root>
      <FormDialog
        formProps={{
          schema: modal.fullDisplay ? patientAddFormSchema : patientAddFormSchemaShort,
          uiSchema: uiPatientAddFormSchema,
          formData: _.cloneDeep(formData),
          onChange: (form) => setFormData(form.formData),
          customValidate: patientAddFormValidate(modal.fullDisplay),
          liveValidate: true
        }}
        onSubmit={submit}
        actions={
          <React.Fragment>
            <MyDialogActions
              onCancel={cancelDial}
              onSubmit={submit}
              submitActionTitle="Ajouter"
              submitLoading={modal.loading}
              SubmitIcon={AddIcon}
              disableSubmit={_.values(patientAddFormGetErrSchema(formData)).some((b) => !!b)}
            />
          </React.Fragment>
        }
        onClose={closeDial}
        open={modal.open}
      />
    </Root>
  )
}

export default BedDialog
