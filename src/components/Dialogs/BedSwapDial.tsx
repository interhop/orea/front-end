import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Dictionary } from '@reduxjs/toolkit'
import _ from 'lodash'

import { Delete as DeleteIcon } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import { Dialog, DialogActions, DialogContent } from '@mui/material'

import MyDialogActions from './MyDialogActions/MyDialogActions'
import { Root } from './styles'
import { displayName } from 'assets/utils'
import { MyBuiForm } from 'components/Form'
import { schemaSwapFormDefault, schemaSwapFormSchema, uiSchemaSwapForm } from 'components/formSchemas'
import { AppDispatch, useAppSelector } from 'state'
import { closeBedSwap } from 'state/modals'
import { closeBedStay, moveBedStay } from 'state/stays'
import { Bed, Patient, Unit } from 'types'

const BedSwapDial: React.FC<any> = () => {
  const { modal, units, beds, stays } = useAppSelector((state) => ({
    modal: state.modals.bedMove,
    units: _.values(state.units.entities),
    beds: state.beds.entities,
    stays: state.stays.entities
  }))
  const dispatch = useDispatch<AppDispatch>()
  const [formData, setFormData] = useState(schemaSwapFormDefault)

  const [closeDialOpen, setCloseDialOpen] = useState<boolean>(false)

  useEffect(() => {
    setFormData(schemaSwapFormDefault)
  }, [modal.stayToMove])

  const openCloseDial = () => setCloseDialOpen(true)

  const cancelCloseDial = () => setCloseDialOpen(false)

  const submitClose = () => {
    modal.stayToMove && dispatch(closeBedStay({ stay: modal.stayToMove }))
    setCloseDialOpen(false)
  }

  const closeDial = () => dispatch(closeBedSwap())

  const cancelDial = () => {
    closeDial()
    setFormData(schemaSwapFormDefault)
  }

  const submit = () =>
    modal.stayToMove && formData.bedId && dispatch(moveBedStay({ stay: modal.stayToMove, newBedId: formData.bedId }))

  const getNewBedStay = () => {
    const bed = _.propertyOf(beds as Dictionary<Bed>)(formData.bedId)
    return bed ? bed.currentBedStay : undefined
  }

  const patient: Patient = modal.stayToMove?.patient as Patient

  if (!modal.stayToMove || !beds) return <></>

  return (
    <Root>
      <Dialog onSubmit={submitClose} onClose={cancelCloseDial} open={closeDialOpen}>
        <DialogContent>Êtes-vous sûr de vouloir retirer le patient: {displayName(patient)} ?</DialogContent>
        <DialogActions>
          <MyDialogActions onCancel={cancelCloseDial} onSubmit={submitClose} submitActionTitle="Retirer le patient" />
        </DialogActions>
      </Dialog>

      <Dialog onSubmit={submit} open={modal.open} onClose={cancelDial}>
        <DialogContent>
          <MyBuiForm
            {...{
              schema: schemaSwapFormSchema(
                modal.stayToMove.patient as Patient,
                units as Unit[],
                beds,
                stays,
                modal.stayToMove
              ),
              uiSchema: uiSchemaSwapForm,
              formData: _.cloneDeep(formData),
              onChange: (form) => setFormData(form.formData),
              liveValidate: false
            }}
          />
        </DialogContent>
        <DialogActions>
          <LoadingButton
            startIcon={<DeleteIcon />}
            onClick={openCloseDial}
            loading={modal.loadingRemove}
            variant="outlined"
            color="warning"
          >
            Enlever
          </LoadingButton>
          <MyDialogActions
            onCancel={cancelDial}
            onSubmit={submit}
            submitActionTitle={getNewBedStay() ? 'Echanger' : 'Déplacer'}
            submitLoading={modal.loadingMove}
            disableSubmit={!formData.bedId || formData.bedId === modal.stayToMove.bed}
          />
        </DialogActions>
      </Dialog>
    </Root>
  )
}

export default BedSwapDial
