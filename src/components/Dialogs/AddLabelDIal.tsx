import React from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { HighlightOff as HighlightOffIcon } from '@mui/icons-material'
import IconButton from '@mui/material/IconButton'
import List from '@mui/material/List'
import ListItemText from '@mui/material/ListItemText'
import ListItem from '@mui/material/ListItem'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import BuiForm from '@rjsf/mui'
import validator from '@rjsf/validator-ajv8'

import { classes, Root } from './styles'
import MyDialogActions from './MyDialogActions/MyDialogActions'
import { AppDispatch, useAppSelector } from 'state'
import { closeLabelAdd } from 'state/modals'
import { updatePatient } from 'state/stays'

const AddLabelDial: React.FC<any> = () => {
  const { modal } = useAppSelector((state) => ({
    modal: state.modals.addLabelDial
  }))
  const defaultListData = (): { [Key: string]: string } =>
    !modal.patient || !modal.field ? {} : _.propertyOf(modal.patient)(modal.field)

  const dispatch = useDispatch<AppDispatch>()
  const [listData, setListData] = React.useState<{ [Key: string]: string } | undefined>()

  const closeDial = () => dispatch(closeLabelAdd())

  const cancelDial = () => {
    closeDial()
    modal.patient && modal.field && setListData(defaultListData)
  }

  const submit = () =>
    modal.patient &&
    modal.field &&
    listData !== undefined &&
    dispatch(
      updatePatient({
        patientId: modal.patient.uuid,
        data: { [modal.field]: listData }
      })
    )

  if (!modal.patient || !modal.field) return <></>

  const dataDisplayed = listData === undefined ? defaultListData() : listData

  return (
    <Root>
      <Dialog onClose={cancelDial} open={modal.open} PaperProps={{ sx: { top: '10%', position: 'fixed' } }}>
        <DialogTitle id="form-dialog-title" className={classes.infoText}>
          Modification de {modal.field}
        </DialogTitle>
        <DialogContent>
          <List>
            {_.entries(dataDisplayed).map(([k, v]) => (
              <ListItem
                key={`list-item-label-dial-${k}`}
                alignItems="flex-start"
                secondaryAction={
                  <IconButton
                    edge="end"
                    aria-label={`remove-label-${k}`}
                    onClick={() => setListData(_.omit(dataDisplayed, [k]))}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                }
              >
                <ListItemText primary={k} secondary={v} />
              </ListItem>
            ))}
          </List>

          <BuiForm
            schema={{
              title: `Ajouter un élément`,
              type: 'object',
              required: ['key'],
              properties: {
                key: {
                  type: 'string',
                  title: 'Type'
                },
                value: {
                  type: 'string',
                  title: 'Détails'
                }
              }
            }}
            onSubmit={({ formData: { key, value } }) => key && setListData({ ...dataDisplayed, [key]: value || '' })}
            customValidate={(fData, errors) => {
              if (_.includes(_.keys(listData).map(_.method('toLowerCase')), fData.key.toLowerCase()))
                errors.addError(`${fData.key} déjà utilisée`)
              return errors
            }}
            validator={validator}
          />
        </DialogContent>
        {listData !== undefined && (
          <DialogActions>
            <MyDialogActions
              onCancel={() => setListData(undefined)}
              onSubmit={submit}
              cancelActionTitle="Rétablir"
              submitLoading={modal.loading}
            />
          </DialogActions>
        )}
      </Dialog>
    </Root>
  )
}

export default AddLabelDial
