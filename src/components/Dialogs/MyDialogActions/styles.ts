import { styled } from '@mui/material/styles'

const PREFIX = 'DialogAction'
export const classes = {
  cancelButton: `${PREFIX}-cancelButton`,
  submitButton: `${PREFIX}-submitButton`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.cancelButton}`]: { margin: theme.spacing(1) },
  [`& .${classes.submitButton}`]: { margin: theme.spacing(1) }
}))
