import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { FormDialog } from 'components/Form'
import { printSettingsFormDefault, printSettingsFormSchema, uiPrintSettingsFormSchema } from 'components/formSchemas'
import { AppDispatch, useAppSelector } from 'state'
import { closePSDial } from 'state/modals'
import { PrintSettings, setPrintSettings } from 'state/settings'
import MyDialogActions from './MyDialogActions/MyDialogActions'

const PrintSettingsDialog: React.FC<any> = () => {
  const { modal, printSettings } = useAppSelector((state) => ({
    modal: state.modals.printSettingsDial,
    printSettings: state.settings.printSettings
  }))
  const dispatch = useDispatch<AppDispatch>()
  const [formData, setFormData] = React.useState<Partial<PrintSettings>>(printSettingsFormDefault)

  useEffect(() => {
    setFormData(printSettings ?? printSettingsFormDefault)
  }, [printSettings])

  const closeDial = () => {
    dispatch(closePSDial())
  }

  const cancelDial = () => {
    setFormData(printSettings ?? printSettingsFormDefault)
    closeDial()
  }

  const submit = () => {
    dispatch(setPrintSettings(formData))
    dispatch(closePSDial())
  }

  return (
    <FormDialog
      formProps={{
        schema: printSettingsFormSchema,
        uiSchema: uiPrintSettingsFormSchema,
        formData: _.cloneDeep(formData),
        onChange: (form) => setFormData(form.formData),
        liveValidate: true
      }}
      onSubmit={submit}
      actions={<MyDialogActions onCancel={cancelDial} onSubmit={submit} />}
      onClose={closeDial}
      open={modal.open}
    />
  )
}

export default PrintSettingsDialog
