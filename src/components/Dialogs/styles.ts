import { styled } from '@mui/material/styles'
import { alpha, Theme } from '@mui/material'

const PREFIX = 'Dialogs'
export const classes = {
  infoText: `${PREFIX}-infoText`,
  mde: `${PREFIX}-mde`,
  link: `${PREFIX}-link`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.mde}`]: {
    backgroundColor: 'transparent !important'
  },
  [`& .${classes.link}`]: {
    marginTop: 4,
    marginBottom: 4,
    padding: 2,
    width: '50%',
    backgroundColor: alpha(theme.palette.secondary.main, 1),
    borderRadius: '12px',
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)',
    textAlign: 'center',
    color: theme.palette.secondary.contrastText
  }
}))

export const easyMdeStyles = (theme: Theme) => `
.CodeMirror {
  background: transparent !important;
  color: ${theme.palette.text.primary} !important;
  caret-color: ${theme.palette.text.primary} !important;
}
.editor-toolbar {
  background-color: ${theme.palette.secondary.light} !important;
}
`
