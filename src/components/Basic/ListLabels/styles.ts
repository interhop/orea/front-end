import { alpha, styled } from '@mui/material/styles'
import convert from 'color-convert'

const getColor = (origin: string, shift: number): string => {
  const secondary = convert.hex.hsl(origin)
  secondary[0] = (secondary[0] + shift * 60) % 360
  return `#${convert.hsl.hex(secondary)}`
}

const PREFIX = 'ListLabels'
export const classes = {
  rootBox: `${PREFIX}-rootBox`,
  avatar: `${PREFIX}-avatar`,
  coloredChip: (shift: number): string => `${PREFIX}-coloredChip-${shift}`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.rootBox}`]: {
    padding: '5px',
    margin: '5px',
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    borderRadius: '6px',
    boxShadow: '0px 4px 6px rgba(0,0,0,0.15)'
  },
  [`& .${classes.coloredChip(0)}`]: {
    backgroundColor: alpha(getColor(theme.palette.secondary.main, 0), 0.15),
    [`& .${classes.avatar}`]: {
      backgroundColor: getColor(theme.palette.secondary.main, 0)
    }
  },
  [`& .${classes.coloredChip(1)}`]: {
    backgroundColor: alpha(getColor(theme.palette.secondary.main, 1), 0.15),
    [`& .${classes.avatar}`]: {
      backgroundColor: getColor(theme.palette.secondary.main, 1)
    }
  },
  [`& .${classes.coloredChip(2)}`]: {
    backgroundColor: alpha(getColor(theme.palette.secondary.main, 2), 0.15),
    [`& .${classes.avatar}`]: {
      backgroundColor: getColor(theme.palette.secondary.main, 2)
    }
  },
  [`& .${classes.coloredChip(3)}`]: {
    backgroundColor: alpha(getColor(theme.palette.secondary.main, 3), 0.15),
    [`& .${classes.avatar}`]: {
      backgroundColor: getColor(theme.palette.secondary.main, 3)
    }
  },
  [`& .${classes.coloredChip(4)}`]: {
    backgroundColor: alpha(getColor(theme.palette.secondary.main, 4), 0.15),
    [`& .${classes.avatar}`]: {
      backgroundColor: getColor(theme.palette.secondary.main, 4)
    }
  },
  [`& .${classes.coloredChip(5)}`]: {
    backgroundColor: alpha(getColor(theme.palette.secondary.main, 5), 0.15),
    [`& .${classes.avatar}`]: {
      backgroundColor: getColor(theme.palette.secondary.main, 5)
    }
  }
}))
