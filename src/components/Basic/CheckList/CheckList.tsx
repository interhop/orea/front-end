import React from 'react'

import { Checkbox, FormControlLabel, Grid, CheckboxProps } from '@mui/material'

import { classes, Root } from './styles'

type CheckListProps = {
  text: string
  onChange: (text: string) => void
}

const CheckList: React.FC<CheckListProps> = ({ text, onChange }) => {
  const regexCheckRow = /- \[[ x]].*/
  const regexChecked = /- \[x].*/
  const values = text.split(`\n`).map((row, i) =>
    !regexCheckRow.test(row)
      ? null
      : {
          id: i,
          label: (regexChecked.test(row) ? row.split('[x]')[1] : row.split('[ ]')[1]).trim(),
          value: regexChecked.test(row)
        }
  )

  const onChangeCheckList = (rowId: number): CheckboxProps['onChange'] => {
    return (event) => {
      const rows = text.split(`\n`)
      const row = rows[rowId]
      if (event.target.checked) {
        rows.splice(rowId, 1, row.replace('[ ]', '[x]'))
      } else {
        rows.splice(rowId, 1, row.replace('[x]', '[ ]'))
      }
      onChange(rows.join(`\n`))
    }
  }

  return (
    <Root>
      <Grid container>
        <Grid item xs={12}>
          {values.map(
            (val) =>
              val && (
                <React.Fragment key={val.id}>
                  <FormControlLabel
                    key={val.id}
                    control={
                      <Checkbox
                        name={val.label}
                        checked={val.value}
                        style={{ padding: '1px', paddingLeft: '9px' }}
                        onChange={onChangeCheckList(val.id)}
                      />
                    }
                    label={val.label}
                    className={classes.infoText}
                  />
                  <br />
                </React.Fragment>
              )
          )}
        </Grid>
      </Grid>
    </Root>
  )
}

export default CheckList
