import { styled } from '@mui/material/styles'

const PREFIX = 'Markdown'
export const classes = {
  text: `${PREFIX}-text`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.text}`]: {
    color: theme.palette.text.primary,
    fontSize: '1rem'
  }
}))
