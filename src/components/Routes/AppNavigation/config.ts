import Login from 'views/Login/Login'
import IntensiveCareService from 'views/IntensiveCareService/IntensiveCareService'
import Home from 'views/Home/Home'
import Bed from 'views/Bed/Bed'
import Accesses from 'views/Accesses/Accesses'
import Issues from 'views/Issues/Issues'

export type RouteConfig = {
  exact: boolean
  displaySideBar: boolean
  path: string
  name: string
  isPrivate: boolean
  component: any
  getPath: (args?: any) => string
}

type RouteNames = 'login' | 'home' | 'intensiveCareServices' | 'intensiveCareService' | 'bed' | 'accesses' | 'issues'

const Config: { [Key in RouteNames]: RouteConfig } = {
  /**
   * Login Page
   */
  login: {
    name: 'login',
    exact: true,
    displaySideBar: false,
    path: '/login',
    isPrivate: false,
    component: Login,
    getPath: (next: string) => (next ? `/login?next=${next}` : '/login')
  },
  /**
   * Main Page
   */
  home: {
    name: 'home',
    exact: true,
    displaySideBar: false,
    path: '/',
    isPrivate: true,
    component: Home,
    getPath: () => '/'
  },
  /**
   * IntensiveCareService Page
   */
  intensiveCareServices: {
    name: 'intensiveCareServices',
    exact: false,
    displaySideBar: true,
    path: '/int-care-services',
    isPrivate: true,
    component: IntensiveCareService,
    getPath: () => '/int-care-services'
  },
  /**
   * IntensiveCareService Page
   */
  intensiveCareService: {
    name: 'intensiveCareService',
    exact: false,
    displaySideBar: true,
    path: '/int-care-services/:serviceId',
    isPrivate: true,
    component: IntensiveCareService,
    getPath: (serviceId: string) => `/int-care-services/${serviceId}`
  },
  /**
   * IntensiveCareService Page
   */
  bed: {
    name: 'bed',
    exact: false,
    displaySideBar: true,
    path: '/beds/:bedId',
    isPrivate: true,
    component: Bed,
    getPath: (bedId: string) => `/beds/${bedId}`
  },
  /**
   * Accesses Page
   */
  accesses: {
    name: 'accesses',
    exact: true,
    displaySideBar: true,
    path: '/accesses',
    isPrivate: true,
    component: Accesses,
    getPath: () => '/accesses'
  },
  /**
   * Issues Page
   */
  issues: {
    name: 'issues',
    exact: true,
    displaySideBar: true,
    path: '/issues',
    isPrivate: true,
    component: Issues,
    getPath: () => '/issues'
  }
  /**
   * Stay
   */
  // {
  //   exact: false,
  //   displaySideBar: true,
  //   path: '/stays',
  //   name: 'stay',
  //   isPrivate: true,
  //   component: Stay
  // },
}

export default Config
