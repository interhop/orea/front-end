import React, { PropsWithChildren } from 'react'
import _ from 'lodash'
import { BrowserRouter as Router, Route, Navigate, Routes } from 'react-router-dom'

import { Theme, useMediaQuery, useTheme } from '@mui/material'
import Grid from '@mui/material/Unstable_Grid2'

import Config, { RouteConfig } from './config'
import AutoLogoutContainer from '../AutoLogout/AutoLogoutContainer'
import LeftMenu from '../LeftMenu/LeftMenu'
import PrivateRoute from '../Private'
import SnackBar from 'components/SnackBar'

type LayoutProps = {
  route: RouteConfig
}

const Layout: React.FC<PropsWithChildren<LayoutProps>> = (props) => {
  const th: Theme = useTheme()
  const smallScreen = useMediaQuery(th.breakpoints.down('sm'))

  return (
    <Grid container width="100%" height="100%" columns={smallScreen ? 12 : 18}>
      {props.route.displaySideBar && <LeftMenu {...props.route} />}

      <Grid xs={0} md={1} height="100%" />

      <Grid
        container
        xs={12}
        sm={16}
        md={15}
        columns={smallScreen ? 12 : 18}
        justifyContent="center"
        height="100%"
        spacing={0}
        sx={{ overflowY: 'scroll' }}
      >
        {props.children}
        {smallScreen && <div style={{ height: '10vh', width: '100%' }} />}
      </Grid>

      <Grid xs={0} md={1} height="100%" />
      <SnackBar />
    </Grid>
  )
}

const AppNavigation = () => (
  <Router>
    <Routes>
      {_.values(Config).map((route, index) => {
        const MyComponent = route.component

        return route.isPrivate ? (
          <Route
            key={index}
            path={route.path}
            element={
              <PrivateRoute>
                <Layout route={route}>
                  <AutoLogoutContainer />
                  <MyComponent />
                </Layout>
              </PrivateRoute>
            }
          />
        ) : (
          <Route
            key={index}
            path={route.path}
            element={
              <Layout route={route}>
                <MyComponent />
              </Layout>
            }
          />
        )
      })}
      <Route element={<Navigate to="/" />} />
    </Routes>
  </Router>
)

export default AppNavigation
