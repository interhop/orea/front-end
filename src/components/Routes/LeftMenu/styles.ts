import { alpha, styled } from '@mui/material/styles'
import Logo from 'assets/images/logoOrea.png'

const PREFIX = 'LeftMenu'

export const subFabTansform = (i: number, total: number): string => `transform-${i}-${total}`

const getAngle = (index: number, total: number, position: 'right' | 'top' = 'right'): number => {
  if (!total) return 0

  const shift = total % 2 === 0 ? Math.PI / (total + 1) : Math.PI / (total + 1)
  const start = position === 'top' ? Math.PI : Math.PI / 2
  return start - index * shift
}

const radialTransform = (scale: number, angle: number, r: number): string =>
  `scale(${scale}) translate(${r * Math.cos(angle)}%, ${-r * Math.sin(angle)}%)`

export const classes = {
  fabBar: `${PREFIX}-fabBar`,
  fabGroup: `${PREFIX}-fabGroup`,
  multiFabContainer: `${PREFIX}-multiFabContainer`,
  drawer: `${PREFIX}-drawer`,
  fab: `${PREFIX}-fab`,
  activated: `${PREFIX}-activated`,
  subFab: `${PREFIX}-subFab`,
  subFabTransform: (i: number, total: number) => subFabTansform(i, total),
  fabIcon: `${PREFIX}-fabIcon`,
  homeIcon: `${PREFIX}-homeIcon`,
  activatedFabIcon: `${PREFIX}-activatedFabIcon`,
  subFabIcon: `${PREFIX}-subFabIcon`
}

export const Root = styled('div')(({ theme }) => ({
  width: 'calc(100% * 1 / var(--Grid-columns))',
  maxWidth: 100,
  backgroundColor: alpha(theme.palette.primary.light, 1),
  height: '100%',
  [theme.breakpoints.down('md')]: {
    width: 'calc(100% * 2 / var(--Grid-columns))'
  },
  [theme.breakpoints.down('sm')]: {
    position: 'fixed',
    backgroundColor: alpha(theme.palette.primary.main, 1),
    bottom: 0,
    zIndex: 20,
    height: 'calc(100% * 1 / var(--Grid-columns))',
    width: '100%',
    maxWidth: '100%'
  },
  [`& .${classes.fabBar}`]: {
    width: '100%',
    height: '100%'
  },
  [`& .${classes.homeIcon}`]: {
    backgroundImage: `url(${Logo})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    aspectRatio: '1/1',
    width: '90%',
    position: 'relative',
    display: 'block',
    cursor: 'pointer',
    margin: 'auto',
    color: theme.palette.secondary.contrastText,
    [theme.breakpoints.down('sm')]: {
      height: '90%',
      width: 'auto'
      // display: 'none'
    }
  },
  [`& .${classes.fabGroup}`]: {
    [theme.breakpoints.down('sm')]: {
      width: 'auto',
      height: '100%'
    }
  },
  [`& .${classes.multiFabContainer}`]: {
    paddingLeft: 0,
    paddingTop: 0,
    marginTop: 4,
    marginBottom: 4,
    position: 'relative',
    maxWidth: '100%',
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      marginTop: 0,
      marginBottom: 0,
      marginLeft: 2,
      marginRight: 2,
      maxWidth: '15%'
    }
  },
  [`& .${classes.fab}`]: {
    aspectRatio: '1/1',
    height: 'auto',
    width: '75%',
    borderRadius: '25%',
    boxShadow: '0px 0px 0px 0px',
    backgroundColor: `${alpha(theme.palette.secondary.main, 1)}`,
    // backgroundColor: theme.palette.background.paper,
    margin: theme.spacing(2, 0, 2, 0),
    ['&:hover']: {
      backgroundColor: `${alpha(theme.palette.secondary.contrastText, 1)}`
    },
    [theme.breakpoints.down('sm')]: {
      maxHeight: '12vh'
    }
  },
  [`& .${classes.activated}`]: {
    backgroundColor: `${alpha(theme.palette.secondary.contrastText, 1)}`
  },
  [`& .${classes.subFab}`]: {
    height: '5vh',
    width: '5vh',
    marginTop: 2,
    marginBottom: 2,
    position: 'absolute',
    backgroundColor: theme.palette.secondary.main
  },
  [`& .${classes.fabIcon}`]: {
    height: '70% !important',
    width: '70% !important',
    color: theme.palette.secondary.contrastText
  },
  [`& .${classes.activatedFabIcon}`]: {
    height: '70% !important',
    width: '70% !important',
    color: theme.palette.secondary.main
  },
  [`& .${classes.subFabIcon}`]: {
    height: '70% !important',
    width: '70% !important',
    color: theme.palette.secondary.contrastText
  },
  [`& .${classes.subFabTransform(1, 4)}`]: {
    transform: radialTransform(1, getAngle(1, 4), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(1, 4, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(2, 4)}`]: {
    transform: radialTransform(1, getAngle(2, 4), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(2, 4, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(3, 4)}`]: {
    transform: radialTransform(1, getAngle(3, 4), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(3, 4, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(4, 4)}`]: {
    transform: radialTransform(1, getAngle(4, 4), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(4, 4, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(1, 3)}`]: {
    transform: radialTransform(1, getAngle(1, 3), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(1, 3, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(2, 3)}`]: {
    transform: radialTransform(1, getAngle(2, 3), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(2, 3, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(3, 3)}`]: {
    transform: radialTransform(1, getAngle(3, 3), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(3, 3, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(1, 2)}`]: {
    transform: radialTransform(1, getAngle(1, 2), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(1, 2, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(2, 2)}`]: {
    transform: radialTransform(1, getAngle(2, 2), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(2, 2, 'top'), 125)
    }
  },
  [`& .${classes.subFabTransform(1, 1)}`]: {
    transform: radialTransform(1, getAngle(1, 1), 125),
    [theme.breakpoints.down('sm')]: {
      transform: radialTransform(1, getAngle(1, 1, 'top'), 125)
    }
  }
}))
