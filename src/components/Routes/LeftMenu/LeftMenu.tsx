import React, { MouseEventHandler } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import _ from 'lodash'

import HelpIcon from '@mui/icons-material/Help'
import LogoutIcon from '@mui/icons-material/ExitToApp'
import NightIcon from '@mui/icons-material/ModeNight'
import ReadIcon from '@mui/icons-material/ChromeReaderMode'
import SunnyIcon from '@mui/icons-material/WbSunny'
import Fab from '@mui/material/Fab'
import Grid from '@mui/material/Unstable_Grid2'
import { Theme, useMediaQuery, useTheme } from '@mui/material'

import { classes, Root } from './styles'
import RoutesConfig, { RouteConfig } from '../AppNavigation/config'
import PrintSettingsDialog from 'components/Dialogs/PrintSettingsDial'
import HelpDialog from 'components/Dialogs/HelpDialog'
import DownloadLink from 'components/pdf/DownloadLink'
import { PrintIcon, PrintParametersIcon } from 'assets/icons'
import { AppDispatch, useAppSelector } from 'state'
import { openHelpDial, openPSDial } from 'state/modals'
import { logout } from 'state/me'
import { toggleNightMode, togglePrintMode, toggleTodoMode } from 'state/settings'

type MenuAction = {
  Icon?: JSX.Element
  activated?: boolean
  onClick?: MouseEventHandler
  subIcons?: { [Key in string]: MenuAction }
}

const LeftMenu: React.FC<RouteConfig> = (route) => {
  const { serviceId } = useParams<{ serviceId?: string }>()
  const navigate = useNavigate()
  const th: Theme = useTheme()
  const smallScreen = useMediaQuery(th.breakpoints.down('sm'))

  const { me, display, services } = useAppSelector((state) => ({
    me: state.me,
    display: state.settings,
    services: state.intensiveCareServices.entities
  }))
  const dispatch = useDispatch<AppDispatch>()

  const actions: { [Key in string]: MenuAction } = {
    'print-mode': {
      Icon: !route.name.includes('intensiveCareService') ? undefined : (
        <PrintIcon className={display.printMode ? classes.activatedFabIcon : classes.fabIcon} />
      ),
      activated: display.printMode,
      onClick: () => dispatch(togglePrintMode()),
      subIcons: !display.printMode
        ? undefined
        : {
            'download-pdf': {
              Icon: (
                <>
                  <DownloadLink service={services[serviceId ?? '']} />
                </>
              )
            },
            'print-settings': {
              Icon: (
                <>
                  <PrintParametersIcon onClick={() => dispatch(openPSDial())} className={classes.subFabIcon} />
                </>
              )
            }
          }
    },
    'todo-mode': {
      Icon: <ReadIcon className={display.todoMode ? classes.activatedFabIcon : classes.fabIcon} />,
      onClick: () => dispatch(toggleTodoMode()),
      activated: display.todoMode
    },
    'night-mode': {
      Icon: display.nightMode ? <SunnyIcon className={classes.fabIcon} /> : <NightIcon className={classes.fabIcon} />,
      onClick: () => dispatch(toggleNightMode())
    },
    help: {
      Icon: <HelpIcon className={classes.fabIcon} />,
      onClick: () => dispatch(openHelpDial())
    },
    logout: {
      Icon: !me.user ? undefined : <LogoutIcon className={classes.fabIcon} />,
      onClick: () => dispatch(logout())
    }
  }

  return (
    <Root>
      {!smallScreen && <span className={classes.homeIcon} onClick={() => navigate(RoutesConfig.home.getPath())} />}
      <Grid container alignItems="center" justifyContent="center" className={classes.fabBar}>
        <Grid container alignItems="center" justifyContent="center" className={classes.fabGroup}>
          {smallScreen && (
            <span className={classes.homeIcon} onClick={() => navigate(RoutesConfig.home.getPath(null))} />
          )}
          {_.entries(actions).map(
            ([k, v]) =>
              v.Icon && (
                <Grid
                  key={k}
                  container
                  justifyContent="center"
                  alignItems="center"
                  className={classes.multiFabContainer}
                >
                  <Fab
                    aria-label={k}
                    className={`${classes.fab} ${v.activated && classes.activated}`}
                    onClick={v.onClick}
                  >
                    {v.Icon}
                  </Fab>
                  {v.subIcons &&
                    _.entries(v.subIcons).map(
                      ([sk, sv], i) =>
                        sv.Icon && (
                          <Fab
                            key={sk}
                            aria-label={sk}
                            className={`${classes.subFab} ${classes.subFabTransform(
                              i + 1,
                              _.filter(_.values(v.subIcons)).length
                            )}`}
                          >
                            {sv.Icon}
                          </Fab>
                        )
                    )}
                </Grid>
              )
          )}
        </Grid>
      </Grid>
      <PrintSettingsDialog />
      <HelpDialog />
    </Root>
  )
}

export default LeftMenu
