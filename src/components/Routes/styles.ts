import { styled } from '@mui/material/styles'

const PREFIX = 'Routes'
export const classes = {
  title: `${PREFIX}-title`,
  validateButton: `${PREFIX}-validateButton`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.title}`]: {
    color: '#153d8a'
  },
  [`& .${classes.validateButton}`]: {
    backgroundColor: '#5BC5F2',
    color: '#FFF',
    borderRadius: '25px',
    marginLeft: theme.spacing(2),
    '&:hover': {
      backgroundColor: '#499cbf'
    }
  }
}))
