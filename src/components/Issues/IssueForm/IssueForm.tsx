import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { LoadingButton } from '@mui/lab'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import CancelIcon from '@mui/icons-material/Cancel'
import Chip from '@mui/material/Chip'
import ExitToApp from '@mui/icons-material/ExitToAppOutlined'
import Grid from '@mui/material/Unstable_Grid2'
import MenuItem from '@mui/material/MenuItem'
import OutlinedInput from '@mui/material/OutlinedInput'
import Select from '@mui/material/Select'
import Typography from '@mui/material/Typography'
import { Theme } from '@mui/material/styles/createTheme'
import { useTheme } from '@mui/material/styles'

import env from 'env'
import { classes, Root } from './styles'
import { FilteredIssuesList } from '../IssuesList/IssuesList'
import useDebounce from 'assets/utils/use-debounce'
import { createIssue } from 'state/issues'
import { AppDispatch } from 'state'
import { IssueLabel, PostedIssue } from 'types'
import RoutesConfig from '../../Routes/AppNavigation/config'

const types = [
  {
    label: "Alerter d'une erreur",
    code: 0
  },
  {
    label: 'Suggérer une amélioration',
    code: 1
  },
  {
    label: 'Envoyer un message',
    code: 2
  }
]

type IssueForm = {
  title: string
  description: string
  issueType: 0 | 1 | 2
  attachment: FileList
}

const defaultFormData: IssueForm = {
  title: '',
  // url: '',
  attachment: FileList as any,
  description: '',
  issueType: 1
}

const featureBody = [
  {
    title: 'Situation actuelle',
    helper: 'Ce qui se passe'
  },
  {
    title: 'Améliorations voulues',
    helper: 'Nouveautés que vous aimeriez'
  }
]

const incidentBody = [
  {
    title: 'Comment reproduire',
    helper: 'Quelle page, quelles actions, navigateur, etc. ?'
  },
  {
    title: 'Comportement attendu',
    helper: 'Ce qui devrait se passer'
  },
  {
    title: 'Résultats non voulus',
    helper: 'Ce qui se passe'
  }
]

const formatBody = (sections: { title: string; helper: string }[]): string => {
  const nextLine = '%0d%0a'

  return sections.map(({ title, helper }) => `%23%23 ${title}${nextLine}(${helper})`).join(nextLine + nextLine)
}

const genEmail = (formData: IssueForm): string => {
  let subject: string
  let body: string
  switch (formData.issueType) {
    case 2:
      subject = 'Message :'
      body = ''
      break
    case 1:
      subject = "J'aimerais voir"
      body = formatBody(featureBody)
      break
    default:
      subject = "J'ai trouvé une erreur"
      body = formatBody(incidentBody)
      break
  }

  return `mailto:${env.REACT_APP_GITLAB_MAILTO}
    ?subject=${subject}
    &body=${body}`
}

const formToIssue = (f: IssueForm): PostedIssue => ({
  title: f.title,
  description: f.description,
  attachment: f.attachment,
  isIncident: f.issueType === 0,
  label: f.issueType === 2 ? '0-Request' : undefined
})

type IssueFormProps = {
  loading?: boolean
  withIssuesLink?: boolean
}

const IssueForm: React.FC<IssueFormProps> = ({ loading, withIssuesLink = false }) => {
  const th: Theme = useTheme()

  const [formData, setFormData] = React.useState<IssueForm>(defaultFormData)
  const [searchTitle, setSearchTitle] = React.useState<string | undefined>()
  const dispatch = useDispatch<AppDispatch>()

  const submit = () => {
    dispatch(createIssue(formToIssue(formData)))
  }

  const _onChangeValue = (key: 'issueType' | 'title' | 'attachment' | 'description', value: any) => {
    setFormData({
      ...formData,
      [key]: value
    })
  }

  const debouncedTitle = useDebounce(700, formData.title)
  useEffect(() => {
    setSearchTitle(debouncedTitle)
  }, [debouncedTitle])

  const error = !formData.title || !formData.description || formData.issueType === undefined
  const errorFiles = formData.attachment?.[0] && formData.attachment[0].size > 10000000

  const openEmail = () => {
    window.open(genEmail(formData), '_blank')
  }

  return (
    <Root>
      <Grid container justifyContent="center" className={classes.card}>
        <Typography variant="h4" className={classes.title}>
          Améliorations en cours
        </Typography>

        {withIssuesLink && (
          <a target="_blank" href={RoutesConfig.issues.getPath(null)} rel="noreferrer" className={classes.emailLink}>
            <Typography variant="h6">
              Voir les améliorations en cours
              <ExitToApp />
            </Typography>
          </a>
        )}

        <Select
          margin="dense"
          onChange={(event) => _onChangeValue('issueType', Number(event.target.value))}
          className={classes.title}
          value={formData.issueType}
          variant="outlined"
        >
          {types.map((label) => (
            <MenuItem key={label.code} value={label.code}>
              {label.label}
            </MenuItem>
          ))}
        </Select>

        <Grid container justifyContent="flex-end" width="100%">
          <Typography variant="body1">
            Vous pouvez aussi{' '}
            <a onClick={openEmail} rel="noreferrer" className={classes.emailLink}>
              envoyer par email
            </a>
          </Typography>
        </Grid>

        <Box className={classes.section}>
          <Typography variant="h5">Sujet</Typography>
          <OutlinedInput
            key="title"
            placeholder="Sujet"
            value={formData.title}
            onChange={(event) => _onChangeValue('title', event.target.value)}
            fullWidth
            sx={{ backgroundColor: 'white' }}
            error={!formData.title}
          />

          <FilteredIssuesList
            type="X-Idea"
            title="Tickets similaires"
            baseIssue={{
              title: searchTitle,
              labels: formToIssue(formData).label ? [formToIssue(formData).label as IssueLabel] : undefined,
              issueType: formToIssue(formData).isIncident ? 'incident' : 'issue'
            }}
          />
        </Box>

        <Box className={classes.section}>
          <Typography variant="h5">Description</Typography>
          <OutlinedInput
            placeholder="Plus de précisions..."
            value={formData.description}
            onChange={(event) => _onChangeValue('description', event.target.value)}
            fullWidth
            multiline
            minRows={5}
            maxRows={8}
            sx={{ backgroundColor: 'white' }}
            error={!formData.description}
          />
        </Box>

        <Box className={classes.section}>
          <Typography variant="h5">Pièce jointe</Typography>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <input
              id="fileInput"
              type="file"
              style={{ display: 'none' }}
              onChange={(event) => _onChangeValue('attachment', event.target?.files)}
            />
            <label htmlFor="fileInput">
              <Button
                variant="outlined"
                component="span"
                color="secondary"
                sx={{ backgroundColor: th.palette.background.paper }}
              >
                Parcourir...
              </Button>
            </label>
            {formData.attachment.length > 0 && (
              <Chip
                sx={{ marginLeft: 8 }}
                label={formData.attachment[0].name}
                onClick={() => _onChangeValue('attachment', FileList as any)}
                icon={<CancelIcon />}
              />
            )}
          </div>
        </Box>

        {errorFiles && <Chip className={classes.errorText} label="La pièce jointe ne doit pas dépasser les 10Mo." />}

        <LoadingButton
          loading={loading}
          variant="contained"
          onClick={submit}
          className={classes.validateButton}
          disabled={error || errorFiles}
        >
          Envoyer
        </LoadingButton>
      </Grid>
    </Root>
  )
}

export default IssueForm
