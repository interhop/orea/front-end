import React from 'react'
import _ from 'lodash'

import Badge from '@mui/material/Badge'
import IconButton, { IconButtonProps } from '@mui/material/IconButton'
import Skeleton from '@mui/material/Skeleton'
import { SvgIconProps } from '@mui/material/SvgIcon'
import Tooltip from '@mui/material/Tooltip'
import Typography from '@mui/material/Typography'

import { JSONSchema7 } from 'json-schema'

import { treatmentLimitationFormSchema } from 'components/formSchemas'
import LatIcon from 'assets/icons/lat_icon'
import { useAppDispatch, useAppSelector } from 'state'
import { openTLDial } from 'state/modals'
import { TreatmentLimitation } from 'types'

const buildBadgeCount = (latData?: TreatmentLimitation): number => {
  if (!latData) return 0

  return _.values(treatmentLimitationFormSchema.properties).reduce(
    (count, props) =>
      count +
      (typeof props === 'boolean' ? 0 : _.keys(props.properties).filter((k) => !!_.propertyOf(latData)(k)).length),
    0
  )
}

const buildInfos = (latData?: TreatmentLimitation): JSX.Element => {
  if (!latData) return <></>

  const latToRows: { txt: string; children: string[] }[] = (
    _.values(treatmentLimitationFormSchema.properties).filter((props) => typeof props !== 'boolean') as JSONSchema7[]
  ).map((props) => ({
    txt: props.title || '',
    children: (
      _.entries(props.properties)
        .filter(([k]) => !!_.propertyOf(latData)(k))
        .filter(([, prop]) => typeof prop !== 'boolean') as [string, JSONSchema7][]
    ).map(([k, prop]) => `- ${prop.type === 'boolean' ? prop.title : prop.title + ':' + _.propertyOf(latData)(k)}`)
  }))

  return (
    <>
      {latToRows
        .filter(({ children }) => children.length)
        .map(({ txt, children }) => (
          <span key={txt}>
            <Typography key={txt} variant="h5">
              {txt}
            </Typography>
            {children.map((c, i) => (
              <Typography key={`${txt}-${i}`} variant="body1">
                {c}
              </Typography>
            ))}
          </span>
        ))}
    </>
  )
}

const getLastTreatmentLimitation = (tls?: TreatmentLimitation[]): TreatmentLimitation | undefined =>
  _.maxBy(tls ?? [], (tl) => new Date(tl.measureDatetime).valueOf())

type TLIconButtonProps = SvgIconProps & {
  stayId: string
  buttonProps?: Partial<IconButtonProps>
}

export const LatIconButton: React.FC<TLIconButtonProps> = ({ stayId, buttonProps, ...props }) => {
  const { stays, display } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings
  }))
  const dispatch = useAppDispatch()
  const stay = stays.entities[stayId]

  const openDial = () => {
    if (display.todoMode) return
    if (!stay) return
    dispatch(
      openTLDial({
        treatmentLimitation: getLastTreatmentLimitation(stay.treatmentLimitations) ?? {
          stay: stayId,
          measureDatetime: _.now().toString()
        }
      })
    )
  }

  if (stays.status === 'loading') return <Skeleton variant="circular" height={50} />
  if (!stay) return <></>

  const tl = getLastTreatmentLimitation(stay.treatmentLimitations)

  return (
    <Tooltip title={buildInfos(tl)}>
      <IconButton onClick={openDial} {...buttonProps}>
        <Badge color="primary" badgeContent={buildBadgeCount(tl)} overlap="rectangular">
          <LatIcon {...props} />
        </Badge>
      </IconButton>
    </Tooltip>
  )
}

export default LatIconButton
