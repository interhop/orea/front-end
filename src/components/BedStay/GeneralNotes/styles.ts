import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'GeneralNotes'
export const classes = {
  card: `${PREFIX}-card`,
  icon: `${PREFIX}-icon`,
  infoText: `${PREFIX}-infoText`,
  title: `${PREFIX}-title`,
  buttonProgress: `${PREFIX}-buttonProgress`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.card}`]: {
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    width: 'auto',
    borderRadius: 12,
    margin: 2,
    padding: 5,
    boxShadow: '0px 8px 12px rgba(0,0,0,0.15)'
  },
  [`& .${classes.icon}`]: {
    color: theme.palette.primary.light,
    fontSize: '1.5rem !important'
  },
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.title}`]: {
    borderRadius: 12,
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    boxShadow: '0px 2px 3px rgba(0,0,0,0.15)',
    padding: 5,
    margin: 5,
    textAlign: 'center'
  },
  [`& .${classes.buttonProgress}`]: {
    color: theme.palette.secondary.main
  }
}))
