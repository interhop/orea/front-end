import React from 'react'
import { Box, Grid, Skeleton } from '@mui/material'

import { classes, Root } from './styles'
import CardTitle from '../CardTitle/CardTitle'
import { FailuresIconsGrids } from 'components/CareService/Bed/common'
import { useAppSelector } from 'state'

const Failures: React.FC<{ stayId: string }> = ({ stayId }) => {
  const { stays } = useAppSelector((state) => ({
    stays: state.stays
  }))

  const stay = stays.entities[stayId]

  if (stays.status === 'loading') return <Skeleton variant="rounded" height={60} />
  if (!stay) return <>no stay in stay = stays.entities</>

  return (
    <Root>
      <Box className={classes.rootBox}>
        <Grid container alignItems="center" justifyContent="center">
          <CardTitle title="Défaillances" />

          <FailuresIconsGrids
            failures={stay.failureMeasures}
            detections={stay.detections}
            displayInactive
            stay={stay}
            IconProps={{ style: { width: '1.5em', height: '1.5em' } }}
          />
        </Grid>
      </Box>
    </Root>
  )
}

export default Failures
