import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'Failures'
export const classes = {
  rootBox: `${PREFIX}-rootBox`,
  boldHeader: `${PREFIX}-boldHeader`,
  infoText: `${PREFIX}-infoText`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.rootBox}`]: {
    padding: theme.spacing(3),
    margin: theme.spacing(3, 0, 3),
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    borderRadius: theme.spacing(3),
    boxShadow: '0px 4px 6px rgba(0,0,0,0.15)'
  },
  [`& .${classes.boldHeader}`]: {
    fontWeight: 'bold',
    marginRight: theme.spacing(3)
  },
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  }
}))
