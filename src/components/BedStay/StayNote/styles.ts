import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'StayNote'
export const classes = {
  card: `${PREFIX}-card`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.card}`]: {
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    width: 'auto',
    borderRadius: 12,
    margin: 2,
    padding: 5,
    boxShadow: '0px 8px 12px rgba(0,0,0,0.15)'
  }
}))
