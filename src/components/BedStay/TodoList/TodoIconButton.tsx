import React from 'react'
import _ from 'lodash'

import { Badge, IconButton, IconButtonProps, Skeleton, SvgIconProps, Tooltip, Typography } from '@mui/material'

import TodoListIcon from 'assets/icons/new_todo_list_icon'
import { howManyUnfilledTasksInMarkdown } from 'assets/utils'
import { useAppDispatch, useAppSelector } from 'state'
import { openMarkdownDial } from 'state/modals'
import { FrontInput, PatientNote, PatientNoteType, Stay } from 'types'

const buildBadgeCount = (todo?: PatientNote): number => {
  if (!todo) return 0

  return howManyUnfilledTasksInMarkdown(todo.content)
}

const buildInfos = (todo?: PatientNote): JSX.Element | null => {
  if (!todo) return null
  const rws = todo.content.split('\n').filter((r) => !r.startsWith('- [x]'))
  if (!rws.length) return <Typography variant="body1">Cliquer pour éditer</Typography>

  return (
    <>
      {rws.map((r, index) => (
        <Typography key={index} variant={r.startsWith('- [ ]') ? 'body1' : 'h6'}>
          {r}
        </Typography>
      ))}
    </>
  )
}

const batchNote = (stay: Stay, type: PatientNoteType): FrontInput<PatientNote> => ({
  stay: stay.uuid,
  type,
  content: ''
})

const getTodoListNote = (notes?: PatientNote[]): PatientNote | undefined => _.find(notes, _.matches({ type: 'todo' }))

type TodoIconButtonProps = SvgIconProps & {
  stayId: string
  buttonProps?: Partial<IconButtonProps>
}

export const TodoIconButton: React.FC<TodoIconButtonProps> = ({ stayId, buttonProps, ...props }) => {
  const { stays, display } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings
  }))
  const dispatch = useAppDispatch()
  const stay = stays.entities[stayId]

  const openDial = () => {
    if (display.todoMode) return
    if (!stay) return
    dispatch(
      openMarkdownDial({
        note: getTodoListNote(stay.notes) ?? batchNote(stay, 'todo')
      })
    )
  }

  if (stays.status === 'loading') return <Skeleton variant="circular" height={50} />
  if (!stay) return <></>

  const todo = getTodoListNote(stay.notes)

  return (
    <Tooltip title={buildInfos(todo)}>
      <IconButton onClick={openDial} {...buttonProps}>
        <Badge
          color="secondary"
          badgeContent={buildBadgeCount(todo)}
          overlap="rectangular"
          slotProps={{
            badge: {
              style: {
                transform: `scale(${buildBadgeCount(todo) ? 1 : 0}) translate(0%, 0%)`
              }
            }
          }}
        >
          <TodoListIcon {...props} />
        </Badge>
      </IconButton>
    </Tooltip>
  )
}

export default TodoIconButton
