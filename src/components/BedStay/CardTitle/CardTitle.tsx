import React from 'react'

import { Chip, SvgIconProps, Typography } from '@mui/material'

import { classes, Root } from './styles'

type CardTitleProps = {
  title: string
  canEdit?: boolean
  onClick?: () => void
  Icon?: React.FC<SvgIconProps>
}

const CardTitle: React.FC<CardTitleProps> = ({ title, onClick, Icon, canEdit = false }) => (
  <Root>
    <Chip
      size="medium"
      label={
        <Typography variant="h6" className={classes.infoText}>
          {title}
        </Typography>
      }
      variant="outlined"
      color={canEdit && onClick ? 'secondary' : 'primary'}
      onClick={canEdit && onClick ? onClick : undefined}
      onDelete={canEdit && onClick ? () => ({}) : undefined}
      deleteIcon={canEdit && onClick && Icon ? <Icon /> : undefined}
      sx={{ margin: 1 }}
    />
  </Root>
)

export default CardTitle
