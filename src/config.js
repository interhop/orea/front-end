import env from 'env'

const baseURL = env.REACT_APP_BASE_URL

const config = {
  axiosConfig: {
    baseURL,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    credentials: true
  },
  devMode: env.REACT_APP_DEV_MODE === 'true',
  apiPath: {
    backendState: 'state',
    login: `accounts/login`,
    refresh: `accounts/refresh`,
    logout: `accounts/logout`,
    users: `users`,
    accesses: `users-app/accesses`,
    beds: 'infra/beds',
    hospitals: 'infra/hospitals',
    intensiveCareServices: 'infra/services',
    units: 'infra/units',
    issues: `voting/issues`,
    patients: 'patients/patients',
    patientStays: 'patients/patient-stays',
    treatmentLimitations: 'patients/treatment-limitations',
    detectionMeasures: 'patients/detection-measures',
    failureMeasures: 'patients/failure-measures',
    patientNotes: 'patients/patient-notes'
  }
}

export default config

export const translate = {
  table: {
    add: 'Ajouter une nouvelle donnée'
  },
  button: {
    add: 'Ajouter',
    change: 'Modifier',
    return: 'Retour'
  }
}
